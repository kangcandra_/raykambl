<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;

class Category extends Model
{

    //
    protected $fillable = ['name', 'slug'];
    public $timestamps = true;
    public function Post()
    {
        return $this->hasMany('App\Models\Post', 'categories_id');
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($category) {
            // mengecek apakah penulis masih punya buku
            if ($category->Post->count() > 0) {
                // menyiapkan pesan error
                $html = 'Category can not be deleted because it still has post : ';
                $html .= '<ul>';
                foreach ($category->Post as $categories) {
                    $html .= "<li>$categories->title</li>";
                }
                $html .= '</ul>';
                Session::flash("flash_notification", [
                    "level" => "danger",
                    "message" => $html,
                ]);
                // membatalkan proses penghapusan
                return false;
            }
        });
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
