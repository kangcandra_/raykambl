<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;
use Illuminate\Database\Eloquent\Factories\HasFactory;
class ProductCategory extends Model
{
    protected $table = 'product_categories';
    protected $fillable = ['name','slug'];


    public function Product()
    {
        return $this->hasMany('App\Models\Product', 'category_id');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($category) {
            // mengecek apakah penulis masih punya buku
            if ($category->Product->count() > 0) {
                // menyiapkan pesan error
                $html = 'Category can not be deleted because it still has Product : ';
                $html .= '<ul>';
                foreach ($category->Product as $categories) {
                    $html .= "<li>$categories->name</li>";
                }
                $html .= '</ul>';
                Session::flash("flash_notification", [
        "level"=>"danger",
        "message"=>$html
        ]);
                // membatalkan proses penghapusan
                return false;
            }
        });
    }
}
