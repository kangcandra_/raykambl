<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
class ProductTag extends Model
{
    protected $table = 'product_tags';
    protected $fillable = ['name','slug'];

    public function Product()
    {
    	return $this->belongsToMany('App\Models\Product');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
