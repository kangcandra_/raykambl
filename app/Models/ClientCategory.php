<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ClientCategory extends Model
{
    protected $table = 'client_categories';
    protected $fillable = ['name','slug'];
    public $timestamps = true;

    public function Client()
    {
    	return $this->hasMany('App\Models\Client','clientcategories_id');
    }

    public static function boot()
	{
		parent::boot();
		self::deleting(function($category) {
		// mengecek apakah penulis masih punya buku
		if ($category->Client->count() > 0) {
		// menyiapkan pesan error
		$html = 'Category can not be deleted because it still has Client : ';
		$html .= '<ul>';
		foreach ($category->Client as $categories) {
		$html .= "<li>$categories->title</li>";
		}
		$html .= '</ul>';
		Session::flash("flash_notification", [
		"level"=>"danger",
		"message"=>$html
		]);
		// membatalkan proses penghapusan
		return false;
		}
		});
	}

	public function getRouteKeyName()
    {
        return 'slug';
    }
}
