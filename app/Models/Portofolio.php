<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
class Portofolio extends Model
{

    protected $fillable = ['title','photo','portofoliocategories_id','slug'];
    public $timestamps = true;
    public function PortofolioCategory()
    {
    	return $this->belongsTo('App\Models\PortofolioCategory','portofoliocategories_id');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
