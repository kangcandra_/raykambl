<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Client extends Model
{
	protected $table = 'clients';
    protected $fillable = ['name','photo','clientcategories_id','slug','description','links'];
    public $timestamps = true;
    public function ClientCategory()
    {
    	return $this->belongsTo('App\Models\ClientCategory','clientcategories_id');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
