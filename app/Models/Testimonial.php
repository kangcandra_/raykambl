<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
class Testimonial extends Model
{
    protected $table = 'testimonials';
    protected $fillable = ['name','description','photo','company'];
    public $timestamps = true;
}
