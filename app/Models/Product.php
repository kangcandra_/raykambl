<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
class Product extends Model
{
    protected $fillable = ['name','photo','model','slug','price','description','categories_id'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function ProductCategory()
    {
        return $this->belongsTo('App\Models\ProductCategory', 'category_id');
    }

    public function ProductTag()
    {
        return $this->belongsToMany('App\Models\ProductTag');
    }

    public function Cart()
    {
        return $this->belongsToMany('App\Models\Cart');
    }

    public function User()
    {
        return $this->belongsToMany('App\Models\User');
    }
}
