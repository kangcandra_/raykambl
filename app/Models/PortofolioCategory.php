<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;
use Illuminate\Database\Eloquent\Factories\HasFactory;
class PortofolioCategory extends Model
{

    //
   	protected $table = 'portofolio_categories';
    protected $fillable = ['name','slug'];
    public $timestamps = true;

    public function Portofolio()
    {
    	return $this->hasMany('App\Models\Portofolio','portofoliocategories_id');
    }

    public static function boot()
	{
		parent::boot();
		self::deleting(function($category) {
		// mengecek apakah penulis masih punya buku
		if ($category->Portofolio->count() > 0) {
		// menyiapkan pesan error
		$html = 'Category can not be deleted because it still has Portofolio : ';
		$html .= '<ul>';
		foreach ($category->Portofolio as $categories) {
		$html .= "<li>$categories->title</li>";
		}
		$html .= '</ul>';
		Session::flash("flash_notification", [
		"level"=>"danger",
		"message"=>$html
		]);
		// membatalkan proses penghapusan
		return false;
		}
		});
	}

	public function getRouteKeyName()
    {
        return 'slug';
    }
}
