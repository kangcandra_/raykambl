<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Division extends Model
{

    protected $fillable = ['name'];
    public $timestamps = true;
    public function Team()
    {
    	return $this->hasMany('App\Models\Team','divisions_id');
    }

    public static function boot()
	{
		parent::boot();
		self::deleting(function($category) {
		// mengecek apakah penulis masih punya buku
		if ($category->Team->count() > 0) {
		// menyiapkan pesan error
		$html = 'Category can not be deleted because it still has Team : ';
		$html .= '<ul>';
		foreach ($category->Team as $categories) {
		$html .= "<li>$categories->name</li>";
		}
		$html .= '</ul>';
		Session::flash("flash_notification", [
		"level"=>"danger",
		"message"=>$html
		]);
		// membatalkan proses penghapusan
		return false;
		}
		});
	}
}
