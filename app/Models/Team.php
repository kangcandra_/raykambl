<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
class Team extends Model
{
	protected $table = 'teams';
	protected $fillable = ['name','photo','fb','tw','yt','gp','divisions_id'];
	public $timestamps = true;

	public function Division()
	{
		return $this->belongsTo('App\Models\Division','divisions_id');
	}
}
