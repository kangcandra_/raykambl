<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
class Tag extends Model
{

    protected $table = 'tags';
    protected $fillable = ['name','slug'];

    public function Post()
    {
    	return $this->belongsToMany('App\Models\Post');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
