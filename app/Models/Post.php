<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Post extends Model
{
    use Searchable;
    use \Conner\Tagging\Taggable;
    protected $fillable = ['title', 'content', 'photo', 'categories_id'];
    protected $visible = ['title', 'content', 'photo', 'categories_id'];

    public function User()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function Category()
    {
        return $this->belongsTo('App\Models\Category', 'categories_id');
    }

    public function Tag()
    {
        return $this->belongsToMany('App\Models\Tag');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
