<?php

namespace App\Providers;

use Auth;
use DB;
use Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);
        view()->composer('frontend.blog.partials.sidebar', function ($view) {
            $tags = \App\Models\Tag::all();
            $category = \App\Models\Category::all();
            $blogdata = \App\Models\Post::orderBy('created_at', 'desc')->take(4)->get();
            $view->with(compact('tags', 'blogdata', 'category'));
        });
        // blog slider
        // view()->composer('frontend.partials.slider',function($view){
        //     $post = \App\Models\Post::orderBy('created_at', 'desc')->take(5)->get();
        //     $view->with(compact('post'));
        // });

        //portofolio slider
        view()->composer('frontend.partials.slider', function ($view) {
            $portofolio = \App\Models\Portofolio::orderBy('created_at', 'desc')->take(5)->get();
            $post = \App\Models\Post::orderBy('created_at', 'desc')->take(5)->get();
            $view->with(compact('portofolio', 'post'));
        });

        view()->composer('backend.index', function ($view) {
            $post = \App\Models\Post::all();
            $product = \App\Models\Product::all();
            $user = \App\Models\User::all();
            $client = \App\Models\Client::all();
            $portofolio = \App\Models\Portofolio::all();
            $view->with(compact('post', 'client', 'product', 'user', 'portofolio'));
        });

        view()->composer('partials.frontend.nav', function ($view) {
            if (Auth::guest()) {
            } else {
                $cart = DB::table('cart')
                    ->join('products', 'products.id', '=', 'cart.product_id')
                    ->join('users', 'cart.user_id', '=', 'users.id')
                    ->select('cart.*', 'products.name as product_name', 'products.price', 'products.model', 'products.photo')
                    ->where('user_id', Auth::user()->id)
                    ->get();
                // dd($cart);
                $view->with(compact('cart'));
            }
        });

        view()->composer('partials.backend.sidebar', function ($view) {
            $post = \App\Models\Post::all();
            $category = \App\Models\Category::all();
            // $tag = \App\Models\Tag::all();
            $product = \App\Models\Product::all();
            $product_categories = \App\Models\ProductCategory::all();
            $product_tag = \App\Models\ProductTag::all();
            $client = \App\Models\Client::all();
            $client_categories = \App\Models\ClientCategory::all();
            $team = \App\Models\Team::all();
            $division = \App\Models\Division::all();
            $portofolio = \App\Models\Portofolio::all();
            $portofoliocategories = \App\Models\PortofolioCategory::all();
            $view->with(compact('post', 'category', 'product', 'product_categories', 'product_tag', 'client', 'client_categories', 'team', 'division', 'portofolio', 'portofoliocategories'));
        });

        Validator::extend('passcheck', function ($attribute, $value, $parameters) {
            return Hash::check($value, $parameters[0]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
