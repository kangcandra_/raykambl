<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class SearchesController extends Controller
{
    public function search(Request $request) {
		$query = $request->get('q');
        // $posts = Post::search($query)->paginate(10);
        $posts = Post::Where('title','like','%'.$query.'%')
						->orWhere('content','like','%'.$query.'%')
						->orWhere('slug','like','%'.$query.'%')->get();
        // dd($posts);
        return view('frontend.search.index', compact('posts', 'query'));
	}
}
