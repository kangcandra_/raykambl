<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Laratrust\LaratrustFacade as Laratrust;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->permission === 'Admin') return $this->adminDashboard();
        if (Laratrust::hasRole('member')) return $this->memberDashboard();
        if (Auth::user()->permission === 'Author') return $this->authorDashboard();

    }
    protected function adminDashboard()
    {
        return view('backend.index');
    }
    protected function authorDashboard()
    {
        return view('backend.index');
    }
    protected function memberDashboard()
    {
        return view('frontend.member.index');
    }
    public function error()
    {
        return view('errors.403');
    }



}
