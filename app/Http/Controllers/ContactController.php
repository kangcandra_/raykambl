<?php

namespace App\Http\Controllers;

use Alert;
use Illuminate\Http\Request;
use Mail;
use Validator;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.contact');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->only('name', 'email', 'pesan', 'phone');

        $rules = array(
            'name' => 'required|max:40',
            'email' => 'required|email',
            'phone' => 'required',
            'pesan' => 'required|min:10|max:300',
            'g-recaptcha-response' => 'required|captcha');

        $pesan = array(
            'name.required' => 'Your Name should not be empty',
            'email.required' => 'Your Email should not be empty',
            'pesan.required' => 'Your Message should not be empty',
            'pesan.min' => 'Your message can not be less than 10 characters',
            'pesan.max' => 'your message can not be more than 300 characters',
            'phone.required' => 'Your Phone should not be empty');

        $validation = Validator::make($request->all(), $rules, $pesan);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        } else {
            Mail::send('emails.contact', $input, function ($message) use ($input) {
                //email 'From' field: Get users email add and name
                $message->from($input['email'], $input['name']);
                //email 'To' field: cahnge this to emails that you want to be notified.
                $message->to('candraherdiansyah14@gmail.com', 'candra')->cc('candraherdiansyah14@gmail.com')->subject('contact request');
            });
            Alert::success('Thanks for contacting us!', 'Good Job')->autoclose(1500);
            return Redirect('contact');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
