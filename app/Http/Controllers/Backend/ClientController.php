<?php

namespace App\Http\Controllers\Backend;

use Alert;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\ClientCategory;
use File;
use Illuminate\Http\Request;
use Str;
use Validator;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $categories = ClientCategory::all();
        $client = Client::latest()->paginate(16);
        return view('backend.client.index', compact('client', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = ClientCategory::all();
        return view('backend.client.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'description' => 'required',
            'photo' => 'required|image|max:2048',
            'clientcategories_id' => 'required',
            'link' => 'required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
        ];

        $message = [
            'name.required' => 'The Name is required',
            'name.unique' => 'Name is already used find another Name',
            'photo.required' => 'The Photo is required',
            'photo.image' => 'Must fill in a photo',
            'photo.max' => 'Maximum capacity of photos 2048kb',
            'description.required' => 'The description is required',
            'link.required' => 'The Link is required',
            'link.regex' => 'Fill with website address',

        ];

        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }

        $client = new Client;
        $client->name = $request->name;
        $client->description = $request->description;
        $client->slug = Str::slug($request->name, '-');
        $client->clientcategories_id = $request->clientcategories_id;
        $client->link = $request->link;

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationPath = public_path() . '/assets/img/client/';
            $filename = str_random(6) . '_' . $file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, $filename);
            $client->photo = $filename;
        }

        // $client->tags()->sync($request->tags, false);

        $client->save();
        Alert::success('Data successfully saved', 'Good Job')->autoclose(1500);
        return redirect()->route('backend.client.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::findOrFail($id);
        return view('backend/client/show', compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::findOrFail($id);
        $categories = ClientCategory::all();
        $selectedCategory = Client::first()->clientcategories_id;
        return view('backend.client.edit', compact('categories', 'client', 'selectedCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|max:255',
            'description' => 'required',
            'photo' => 'image|max:2048',
            'clientcategories_id' => 'required',
        ];

        $message = [
            'name.required' => 'The name is required',
            'name.unique' => 'name is already used find another name',
            'photo.required' => 'The Photo is required',
            'photo.image' => 'Must fill in a photo',
            'photo.max' => 'Maximum capacity of photos 2048kb',
            'description.required' => 'The description is required',
            'link.required' => 'The Link is required',
            'link.regex' => 'Fill with website address',

        ];

        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }

        $client = client::findOrFail($id);
        $client->name = $request->name;
        $client->description = $request->description;
        $client->slug = Str::slug($request->name, '-');
        $client->clientcategories_id = $request->clientcategories_id;

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationPath = public_path() . '/assets/img/client/';
            $filename = str_random(6) . '_' . $file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, $filename);

            // hapus photo lama, jika ada
            if ($client->photo) {
                $old_photo = $client->photo;
                $filepath = public_path() . DIRECTORY_SEPARATOR . 'assets/img/testimonial'
                . DIRECTORY_SEPARATOR . $client->photo;
                try {
                    File::delete($filepath);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }
            }
            $client->photo = $filename;
        }
        $client->save();
        Alert::success('Data successfully saved', 'Good Job')->autoclose(1500);
        return redirect()->route('backend.client.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::findOrFail($id);

        if ($client->photo) {
            $old_photo = $client->photo;
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'assets/img/client/'
            . DIRECTORY_SEPARATOR . $client->photo;
            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
        }
        $client->delete();
        Alert::success('Data successfully deleted', 'Good Job')->autoclose(1000);
        return redirect()->route('backend.client.index');
    }
}
