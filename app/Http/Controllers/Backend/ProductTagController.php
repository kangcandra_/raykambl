<?php

namespace App\Http\Controllers\Backend;

use Alert;
use App\Http\Controllers\Controller;
use App\Models\ProductTag;
use Illuminate\Http\Request;
use Str;
use Validator;

class ProductTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $product_tag = ProductTag::orderBy('id', 'asc')->paginate(12);
        // dd($product_tag);
        return view('backend.product_tag.index', compact('product_tag'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255|unique:product_tags',
        ];

        $message = [
            'name.required' => 'The Name Tag is required',
            'name.unique' => 'Category is already used find another tag',
        ];

        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }
        $product_tag = new ProductTag;
        $product_tag->name = $request->name;
        $product_tag->slug = Str::slug($request->name, '-');
        $product_tag->save();
        Alert::success('Data successfully saved', 'Good Job')->autoclose(1500);
        return redirect()->route('backend.product_tag.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|max:255',
        ];

        $message = [
            'name.required' => 'The Name Category is required',
        ];

        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }
        $product_tag = ProductTag::findOrFail($id);
        $product_tag->name = $request->name;
        $product_tag->slug = Str::slug($request->name, '-');
        $product_tag->save();
        Alert::success('Data successfully edited', 'Good Job')->autoclose(1500);
        return redirect()->route('backend.product_tag.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductTag::findOrFail($id)->delete();
        Alert::success('Data successfully deleted', 'Good Job')->autoclose(1500);
        return back();
    }
}
