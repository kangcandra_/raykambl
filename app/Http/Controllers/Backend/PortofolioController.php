<?php

namespace App\Http\Controllers\Backend;

use Alert;
use App\Http\Controllers\Controller;
use App\Models\Portofolio;
use App\Models\                     PortofolioCategory;
use File;
use Illuminate\Http\Request;
use Str;
use Validator;

class PortofolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $categories = PortofolioCategory::all();
        $portofolio = Portofolio::latest()->paginate(12);
        return view('backend.portofolio.index', compact('portofolio', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = PortofolioCategory::all();
        return view('backend.portofolio.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|max:255|unique:portofolios',
            'photo' => 'image|max:2048',
            'portofoliocategories_id' => 'required',
        ];

        $message = [
            'title.required' => 'The Title is required',
            'title.unique' => 'Title is already used find another Title',
            'photo.required' => 'The Photo is required',
            'photo.image' => 'Must fill in a photo',
            'photo.max' => 'Maximum capacity of photos 2048kb',

        ];

        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }

        $portofolio = new Portofolio;
        $portofolio->title = $request->title;
        $portofolio->slug = Str::slug($request->title, '-');
        $portofolio->portofoliocategories_id = $request->portofoliocategories_id;

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationPath = public_path() . '/assets/img/portofolio/';
            $filename = str_random(6) . '_' . $file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, $filename);
            $portofolio->photo = $filename;
        }

        // $portofolio->tags()->sync($request->tags, false);

        $portofolio->save();

        Alert::success('Data successfully saved', 'Good Job')->autoclose(1500);
        return redirect()->route('backend.portofolio.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $portofolio = Portofolio::findOrFail($id);
        return view('backend/portofolio/show', compact('portofolio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $portofolio = Portofolio::findOrFail($id);
        $categories = PortofolioCategory::all();
        $selectedCategory = Portofolio::findOrFail($id)->portofoliocategories_id;
        return view('backend.portofolio.edit', compact('categories', 'portofolio', 'selectedCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'title' => 'required|max:255',
            'photo' => 'image|max:2048',
            'portofoliocategories_id' => 'required',
        ];

        $message = [
            'title.required' => 'The Title is required',
            'photo.required' => 'The Title is required',
            'photo.image' => 'Must fill in a photo',
            'photo.max' => 'Maximum capacity of photos 2048kb',
            'categories_id.required' => 'The Category is required',

        ];

        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }

        $portofolio = Portofolio::findOrFail($id);
        $portofolio->title = $request->title;
        $portofolio->slug = Str::slug($request->title, '-');
        $portofolio->portofoliocategories_id = $request->portofoliocategories_id;

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationPath = public_path() . '/assets/img/portofolio/';
            $filename = str_random(6) . '_' . $file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, $filename);

            // hapus photo lama, jika ada
            if ($portofolio->photo) {
                $old_photo = $portofolio->photo;
                $filepath = public_path() . DIRECTORY_SEPARATOR . '/assets/img/portofolio'
                . DIRECTORY_SEPARATOR . $portofolio->photo;
                try {
                    File::delete($filepath);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }
            }
            $portofolio->photo = $filename;
        }
        $portofolio->save();
        Alert::success('Data successfully saved', 'Good Job')->autoclose(1500);
        return redirect()->route('backend.portofolio.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $portofolio = Portofolio::findOrFail($id);

        if ($portofolio->photo) {
            $old_photo = $portofolio->photo;
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'assets/img/portofolio/'
            . DIRECTORY_SEPARATOR . $portofolio->photo;
            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
        }
        $portofolio->delete();
        Alert::success('Data successfully deleted', 'Good Job')->autoclose(1500);
        return redirect()->route('backend.portofolio.index');
    }
}
