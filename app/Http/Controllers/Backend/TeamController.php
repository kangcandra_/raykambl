<?php

namespace App\Http\Controllers\Backend;

use Alert;
use App\Http\Controllers\Controller;
use App\Models\Division;
use App\Models\Team;
use File;
use Illuminate\Http\Request;
use Validator;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $division = Division::all();
        $team = Team::orderBy('id', 'asc')->paginate(8);
        // $selectedCategory = Team::select('id','categories_id');
        return view('backend.team.index', compact('team', 'division', 'selectedCategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illu minate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'photo' => 'required|image|max:2048',
            'divisions_id' => 'required',
            'fb' => 'nullable',
            'yt' => 'nullable',
            'tw' => 'nullable',
            'gp' => 'nullable',
        ];

        $message = [
            'name.required' => 'The Name is required',
            'photo.required' => 'The Title is required',
            'photo.image' => 'Must fill in a photo',
            'photo.max' => 'Maximum capacity of photos 2048kb',
            'divisions_id.required' => 'The Division is required',

        ];

        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }

        $team = new Team;
        $team->name = $request->name;
        $team->divisions_id = $request->divisions_id;
        $team->yt = $request->yt;
        $team->fb = $request->fb;
        $team->gp = $request->gp;
        $team->tw = $request->tw;

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationPath = public_path() . '/assets/img/team/';
            $filename = str_random(6) . '_' . $file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, $filename);
            $team->photo = $filename;
        }
        $team->save();

        Alert::success('Data successfully saved', 'Good Job')->autoclose(1500);
        return redirect()->route('backend.team.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|max:255',
            'photo' => 'image|max:2048',
            'divisions_id' => 'required',
            'fb' => 'nullable',
            'yt' => 'nullable',
            'tw' => 'nullable',
            'gp' => 'nullable',
        ];

        $message = [
            'name.required' => 'The Name is required',
            'photo.image' => 'Must fill in a photo',
            'photo.max' => 'Maximum capacity of photos 2048kb',
            'divisions_id.required' => 'The Division is required',

        ];

        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }

        $team = Team::findOrFail($id);
        $team->name = $request->name;
        $team->divisions_id = $request->divisions_id;
        $team->yt = $request->yt;
        $team->fb = $request->fb;
        $team->gp = $request->gp;
        $team->tw = $request->tw;

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationPath = public_path() . '/assets/img/team/';
            $filename = str_random(6) . '_' . $file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, $filename);

            // hapus photo lama, jika ada
            if ($team->photo) {
                $old_photo = $team->photo;
                $filepath = public_path() . DIRECTORY_SEPARATOR . 'assets/img/team'
                . DIRECTORY_SEPARATOR . $team->photo;
                try {
                    File::delete($filepath);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }
            }
            $team->photo = $filename;
        }
        $team->save();
        Alert::success('Data successfully saved', 'Good Job')->autoclose(1500);
        return redirect()->route('backend.team.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team = Team::findOrFail($id);

        if ($team->photo) {
            $old_photo = $team->photo;
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'assets/img/team/'
            . DIRECTORY_SEPARATOR . $team->photo;
            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
        }
        $team->delete();
        Alert::success('Data successfully deleted', 'Good Job')->autoclose(1500);
        return redirect()->route('backend.team.index');
    }
}
