<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Map;
use Alert;
use Validator;
use Auth;
use FarhanWazir\GoogleMaps\GMaps;

class MapsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $maps = Map::where('user_id', Auth::user()->id)->get();
        return view('backend.maps.index', compact('maps'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.maps.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $map = new Map;
        $map->user_id = Auth::user()->id;
        $map->title = $request->title;
        $map->address = $request->location;
        $map->radius = $request->radius;
        $map->latitude = $request->lat;
        $map->longitude = $request->long;
        $map->save();
        return redirect()->route('backend.maps.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Map $map)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Map $map)
    {
        return view('backend.maps.edit', compact('map'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Map $map)
    {
        $map->user_id   = Auth::user()->id;
        $map->title     = $request->title;
        $map->address   = $request->location;
        $map->radius    = $request->radius;
        $map->latitude  = $request->lat;
        $map->longitude = $request->long;
        $map->save();
        return redirect()->route('backend.maps.destroy');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Map $map)
    {
        $map->delete();
        return redirect()->route('backend.maps.index');
    }

    public function cekmap()
    {
        $maps = Map::all();

        $gmap = new GMaps();

        foreach ($maps as $map) {
            $config = array();
            $config['center'] = $map->latitude. ','. $map->longitude;
            $config['zoom'] = '14';
            $config['map_height'] = '500px';
            $config['scrollwheel'] = false;

            $gmap->initialize($config);

            //Add Marker
            $marker['position'] = $map->latitude. ','. $map->longitude;
            $marker['infowindow_content'] = $map->title;
            $marker['icon'] = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';

            $gmap->add_marker($marker);
        }


        $map = $gmap->create_map();

        return view('cekmap', compact('map'));
    }
}
