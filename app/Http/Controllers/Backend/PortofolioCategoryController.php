<?php

namespace App\Http\Controllers\Backend;

use Alert;
use App\Http\Controllers\Controller;
use App\Models\PortofolioCategory;
use Illuminate\Http\Request;
use Str;
use Validator;

class PortofolioCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $portofoliocategories = PortofolioCategory::latest()->paginate(12);
        return view('backend.portofolio_category.index', compact('portofoliocategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255|unique:portofolio_categories',
        ];

        $message = [
            'name.required' => 'The Name Category is required',
            'name.unique' => 'Category is already used find another category',
        ];

        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }
        $portofoliocategories = new PortofolioCategory;
        $portofoliocategories->name = $request->name;
        $portofoliocategories->slug = Str::slug($request->name, '-');
        $portofoliocategories->save();
        Alert::success('Data successfully saved', 'Good Job')->autoclose(1500);
        return redirect()->route('backend.portofolio_categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|max:255',
        ];

        $message = [
            'name.required' => 'The Name Category is required',
        ];

        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }
        $portofoliocategories = PortofolioCategory::findOrFail($id);
        $portofoliocategories->name = $request->name;
        $portofoliocategories->slug = Str::slug($request->name, '-');
        $portofoliocategories->save();
        Alert::success('Data successfully edited', 'Good Job')->autoclose(1500);
        return redirect()->route('backend.portofolio_categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PortofolioCategory::findOrFail($id)->delete();
        Alert::success('Data successfully deleted', 'Good Job')->autoclose(1500);
        return back();
    }
}
