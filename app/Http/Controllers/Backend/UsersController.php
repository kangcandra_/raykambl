<?php

namespace App\Http\Controllers\Backend;

use Alert;
use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Validator;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index()
    {
        if (Auth::user()->permission === 'Admin') {
            $users = User::orderBy('id', 'asc')->paginate(12);
            return view('backend.users.index', compact('users'));
        } else {
            return view('errors.403');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'permission' => 'required',
            'password' => 'required|min:6|confirmed',
        ];

        $message = [
            'name.required' => 'The Name field is required',
            'name.max' => 'Your name can not be of 255 characters',
            'emai.required' => 'The Email field is required',
            'email.max' => 'Your Email can not be of 255 characters',
            'email.unique' => 'Email is already used find another email',
            'permission.required' => 'The Permission field is required',
            'password.required' => 'The Password field is required',
            'password.min' => 'the contents of your password should be more than 6 characters',
        ];
        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }
        $this->validate($request, $rules, $message);
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->permission = $request->permission;
        $user->password = bcrypt($request->password);
        $user->save();
        Alert::success('Data successfully saved', 'Good Job')->autoclose(1000);
        return redirect()->route('backend.users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'permission' => 'required',
            'password' => 'required|min:6|confirmed',
        ];

        $message = [
            'name.required' => 'The Name field is required',
            'name.max' => 'Your name can not be of 255 characters',
            'emai.required' => 'The Email field is required',
            'email.max' => 'Your Email can not be of 255 characters',
            'permission.required' => 'The Permission field is required',
            'password.required' => 'The Password field is required',
            'password.min' => 'the contents of your password should be more than 6 characters',
        ];
        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }
        $this->validate($request, $rules, $message);
        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->permission = $request->permission;
        $user->password = bcrypt($request->password);
        $user->save();
        Alert::success('Data successfully saved', 'Good Job')->autoclose(1500);
        return redirect()->route('backend.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::findOrFail($id)->delete();
        Alert::success('Data successfully deleted', 'Good Job')->autoclose(1500);
        return redirect()->route('backend.users.index');
    }
}
