<?php

namespace App\Http\Controllers\Backend;

use Alert;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductTag;
use File;
use Illuminate\Http\Request;
use Str;
use Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $categories = ProductCategory::all();
        $products = Product::paginate(15);
        $selected = $products->ProductCategory->pluck('id')->toArray()->get();
        dd($selected);
        return view('backend.product.index', compact('products', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = ProductTag::all();
        $categories = ProductCategory::all();
        return view('backend.product.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255|unique:products',
            'photo' => 'required|image|max:2048',
            'category_id' => 'required',
            'price' => 'numeric|required',
            'tags' => 'required',
            'model' => 'required',
            'description' => 'required',
        ];

        $message = [
            'name.required' => 'The name is required',
            'name.unique' => 'name is already used find another name',
            'photo.required' => 'The photo is required',
            'photo.image' => 'Must fill in a photo',
            'photo.max' => 'Maximum capacity of photos 2048kb',
            'category_id.required' => 'The Category is required',
            'tags.required' => 'The Tag is required',
            'model.required' => 'The Tag is required',

        ];

        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }

        $product = new Product;
        $product->name = $request->name;
        $product->slug = Str::slug($request->name, '-');
        $product->model = $request->model;
        $product->price = $request->price;
        $product->description = $request->description;
        $product->category_id = $request->category_id;

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationPath = public_path() . '/assets/img/product';
            $filename = str_random(6) . '_' . $file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, $filename);
            $product->photo = $filename;
        }

        // $product->tags()->sync($request->tags, false);

        $product->save();
        $product->ProductTag()->attach($request->tags);

        Alert::success('Data successfully saved', 'Good Job')->autoclose(1000);
        return redirect()->route('backend.product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        $tags = ProductTag::all();
        return view('backend/product/show', compact('product', 'tags'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $categories = ProductCategory::all();
        $selectedCategory = Product::findOrFail($id)->category_id;
        $selected = $product->ProductTag->pluck('id')->toArray();
        $tags = ProductTag::all();
        // dd($selected);
        return view('backend.product.edit', compact('categories', 'product', 'selected', 'selectedCategory', 'categories', 'tags'));
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|max:255',
            'photo' => 'image|max:2048',
            'category_id' => 'required',
            'price' => 'numeric|required',
            'tags' => 'required',
            'model' => 'required',
        ];

        $message = [
            'name.required' => 'The name is required',
            'name.unique' => 'name is already used find another name',
            'photo.image' => 'Must fill in a photo',
            'photo.max' => 'Maximum capacity of photos 2048kb',
            'category_id.required' => 'The Category is required',
            'tags.required' => 'The Tag is required',
            'model.required' => 'The Tag is required',

        ];

        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }

        $product = product::findOrFail($id);
        $product->name = $request->name;
        $product->slug = Str::slug($request->name, '-');
        $product->model = $request->model;
        $product->price = $request->price;
        $product->description = $request->description;
        $product->category_id = $request->category_id;

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationPath = public_path() . '/assets/img/product';
            $filename = str_random(6) . '_' . $file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, $filename);

            // hapus photo lama, jika ada
            if ($product->photo) {
                $old_photo = $product->photo;
                $filepath = public_path() . DIRECTORY_SEPARATOR . 'assets/img/product'
                . DIRECTORY_SEPARATOR . $product->photo;
                try {
                    File::delete($filepath);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }
            }
            $product->photo = $filename;
        }

        // $product->tags()->sync($request->tags, false);

        $product->save();
        $product->ProductTag()->sync($request->tags);

        Alert::success('Data successfully saved', 'Good Job')->autoclose(1000);
        return redirect()->route('backend.product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);

        if ($product->photo) {
            $old_photo = $product->photo;
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'assets/img/product'
            . DIRECTORY_SEPARATOR . $product->photo;
            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
        }
        $product->delete();
        Alert::success('Data successfully deleted', 'Good Job')->autoclose(1000);
        return redirect()->route('backend.product.index');
    }

    public function addAttribute(Request $request, $id = null)
    {
        $tags = ProductTag::all();
        $categories = ProductCategory::all();
        $productDetails = Product::where(['id' => $id])->first();
        return view('backend.product.add-attribute', compact('productDetails', 'tags', 'categories'));
    }
}
