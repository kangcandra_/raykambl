<?php

namespace App\Http\Controllers\Backend;

use Alert;
use App\Http\Controllers\Controller;
use App\Models\Testimonial;
use File;
use Illuminate\Http\Request;
use Validator;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        // $categories = Category::all();
        $testimonial = Testimonial::latest()->paginate(6);
        return view('backend.testimonial.index', compact('testimonial'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illu minate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'photo' => 'required|image|max:2048',
            'description' => 'required',
            'company' => 'required',
        ];

        $message = [
            'name.required' => 'The Name is required',
            'photo.required' => 'The Title is required',
            'photo.image' => 'Must fill in a photo',
            'photo.max' => 'Maximum capacity of photos 2048kb',
            'description.required' => 'The Description is required',
            'company.required' => 'The Company is required',

        ];

        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }

        $testimonial = new Testimonial;
        $testimonial->name = $request->name;
        $testimonial->description = $request->description;
        $testimonial->company = $request->company;

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationPath = public_path() . '/assets/img/testimonial/';
            $filename = str_random(6) . '_' . $file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, $filename);
            $testimonial->photo = $filename;
        }
        $testimonial->save();

        Alert::success('Data successfully saved', 'Good Job')->autoclose(1500);
        return redirect()->route('backend.testimonial.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|max:255',
            'photo' => 'image|max:2048',
            'description' => 'required',
            'company' => 'required',
        ];

        $message = [
            'name.required' => 'The Name is required',
            'photo.image' => 'Must fill in a photo',
            'photo.max' => 'Maximum capacity of photos 2048kb',
            'description.required' => 'The Description is required',
            'company.required' => 'The Company is required',

        ];

        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }

        $testimonial = Testimonial::findOrFail($id);
        $testimonial->name = $request->name;
        $testimonial->description = $request->description;
        $testimonial->company = $request->company;

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationPath = public_path() . '/assets/img/testimonial/';
            $filename = str_random(6) . '_' . $file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, $filename);

            // hapus photo lama, jika ada
            if ($testimonial->photo) {
                $old_photo = $testimonial->photo;
                $filepath = public_path() . DIRECTORY_SEPARATOR . 'assets/img/testimonial'
                . DIRECTORY_SEPARATOR . $testimonial->photo;
                try {
                    File::delete($filepath);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }
            }
            $testimonial->photo = $filename;
        }
        $testimonial->save();
        Alert::success('Data successfully saved', 'Good Job')->autoclose(1500);
        return redirect()->route('backend.testimonial.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testimonial = Testimonial::findOrFail($id);

        if ($testimonial->photo) {
            $old_photo = $testimonial->photo;
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'assets/img/testimonial/'
            . DIRECTORY_SEPARATOR . $testimonial->photo;
            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
        }
        $testimonial->delete();
        Alert::success('Data successfully deleted', 'Good Job')->autoclose(1500);
        return redirect()->route('backend.testimonial.index');
    }
}
