<?php

namespace App\Http\Controllers\Backend;

use Alert;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Auth;
use File;
use Illuminate\Http\Request;
use Str;
use Validator;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $categories = Category::all();
        $posts = Post::latest()->paginate(12);
        return view('backend.post.index', compact('posts', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('backend.post.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|max:255|unique:posts',
            'content' => 'required',
            'photo' => 'required|image|max:2048',
            'categories_id' => 'required',
            'tags' => 'required',
        ];

        $message = [
            'title.required' => 'The Title is required',
            'title.unique' => 'Title is already used find another Title',
            'photo.required' => 'The Photo is required',
            'photo.image' => 'Must fill in a photo',
            'photo.max' => 'Maximum capacity of photos 2048kb',
            'categories_id.required' => 'The Category is required',
            'tags.required' => 'The Tag is required',

        ];

        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }

        $user = Auth::user();
        $post = new Post;
        $post->user_id = $user->id;
        $post->title = $request->title;

        $post->slug = Str::slug($request->title, '-');
        $post->content = $request->content;
        $post->categories_id = $request->categories_id;

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationPath = public_path() . '/assets/img/post';
            $filename = str_random(6) . '_' . $file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, $filename);
            $post->photo = $filename;
        }

        // $post->tags()->sync($request->tags, false);
        $tags = explode(",", $request->tags);
        $post->save();
        $post->tag($tags);

        // $post->Tag()->attach($request->tags);

        Alert::success('Data successfully saved', 'Good Job')->autoclose(1000);
        return redirect()->route('backend.post.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        $tags = Tag::all();
        return view('backend/post/show', compact('post', 'tags'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $categories = Category::all();
        $selectedCategory = Post::findOrFail($id)->categories_id;
        $selected = $post->Tag->pluck('id')->toArray();
        $tags = Tag::all();
        return view('backend.post.edit', compact('categories', 'tags', 'post', 'selectedCategory', 'selected'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'title' => 'required|max:255',
            'content' => 'required',
            'photo' => 'image|max:2048',
            'categories_id' => 'required',
            'tags' => 'required',
        ];

        $message = [
            'title.required' => 'The Title is required',
            // 'photo.required' => 'The Title is required',
            'photo.image' => 'Must fill in a image',
            'photo.max' => 'Maximum capacity of photos 2048kb',
            'categories_id.required' => 'The Category is required',
            'tags.required' => 'The Tag is required',

        ];

        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }

        $user = Auth::user();
        $post = Post::findOrFail($id);
        $post->user_id = $user->id;
        $post->title = $request->title;

        $post->slug = Str::slug($request->title, '-');
        $post->content = $request->content;
        $post->categories_id = $request->categories_id;

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationPath = public_path() . '/assets/img/post';
            $filename = str_random(6) . '_' . $file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, $filename);

            // hapus photo lama, jika ada
            if ($post->photo) {
                $old_photo = $post->photo;
                $filepath = public_path() . DIRECTORY_SEPARATOR . 'assets/img/post'
                . DIRECTORY_SEPARATOR . $post->photo;
                try {
                    File::delete($filepath);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }
            }
            $post->photo = $filename;
        }

        // $post->tags()->sync($request->tags, false);

        $post->save();
        $post->Tag()->sync($request->tags);

        Alert::success('Data successfully saved', 'Good Job')->autoclose(1000);
        return redirect()->route('backend.post.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = post::findOrFail($id);

        if ($post->photo) {
            $old_photo = $post->photo;
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'assets/img/post'
            . DIRECTORY_SEPARATOR . $post->photo;
            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
        }
        $post->delete();
        Alert::success('Data successfully deleted', 'Good Job')->autoclose(1000);
        return redirect()->route('backend.post.index');
    }
}
