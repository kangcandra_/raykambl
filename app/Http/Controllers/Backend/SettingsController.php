<?php

namespace App\Http\Controllers\Backend;

use Alert;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Session;

class SettingsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile()
    {
        return view('backend.settings.index');
    }
    public function UpdateProfile(Request $request)
    {
        $user = Auth::user();
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $user->id,
            'password' => 'required|passcheck:' . $user->password,
            'new_password' => 'required|confirmed|min:6',
        ], [
            'password.passcheck' => 'Password lama tidak sesuai',
        ]);

        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('new_password'));
        $user->save();
        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Profil berhasil diubah",
        ]);
        Alert::success('Data successfully saved', 'Good Job')->autoclose(2000);
        return redirect()->route('backend.settings.index');
    }
}
