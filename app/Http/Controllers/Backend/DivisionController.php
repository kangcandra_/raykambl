<?php

namespace App\Http\Controllers\Backend;

use Alert;
use App\Http\Controllers\Controller;
use App\Models\Division;
use Illuminate\Http\Request;
use Validator;

class DivisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $division = Division::orderBy('created_at', 'asc')->paginate(12);
        return view('backend.division.index', compact('division'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255|unique:divisions',
        ];

        $message = [
            'name.required' => 'The Name Division is required',
            'name.unique' => 'Division is already used find another division',
        ];

        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }
        $division = new Division;
        $division->name = $request->name;
        $division->save();
        Alert::success('Data successfully saved', 'Good Job')->autoclose(1500);
        return redirect()->route('backend.team_division.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|max:255',
        ];

        $message = [
            'name.required' => 'The Name Division is required',
        ];

        $validation = Validator::make($request->all(), $rules, $message);
        if ($validation->fails()) {
            Alert::error('Sorry your data is invalid, Please try again!', 'Oops!')->persistent("Ok");
            return back()->withErrors($validation)->withInput();
        }
        $division = Division::findOrFail($id);
        $division->name = $request->name;
        $division->save();
        Alert::success('Data successfully edited', 'Good Job')->autoclose(1500);
        return redirect()->route('backend.team_division.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Division::destroy($id)) {
            Alert::error('Division can not be deleted because it still has post!', 'Oops!')->persistent("Ok");
            return redirect()->back();
        } else {
            Alert::success('Data successfully deleted', 'Good Job')->autoclose(1500);
        }
        return back();
    }
}
