<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Cart;
use Auth;
use Alert;
use DB;
use Session;
use Cookie;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addToCart(Request $request)
    {
        if (Auth::guest()) {
            Alert::warning('Please login first to add to the basket', 'Sorry');
            return redirect()->back();
        } elseif (Auth::check()) {
            $id = $request->get('id');
            $qty = $request->get('quantity');
            $count = Cart::where('product_id', '=', $id)->where('user_id', '=', Auth::user()->id)->count();
            if ($count) {
                Alert::warning('The product already in your cart', 'Sorry');
                return redirect()->back();
            }
            $cart = new Cart;
            $cart->user_id = Auth::user()->id;
            $cart->product_id = $id;
            $cart->quantity = $qty;
            $cart->save();
            Alert::success('Data successfully saved to cart', 'Good Job')->autoclose(1500);
            return redirect('cart');
        }

        // $this->validate($request, [
        //         'id' => 'required|exists:products,id',
        //         'quantity' => 'required|integer|min:1'
        //     ]);

        // $product = Product::find($request->get('id'));
        // $quantity = $request->get('quantity');
        // $cart = $request->cookie('cart', []);

        // if (array_key_exists($product->id, $cart)) {
        //     $quantity += $cart[$product->id];
        // }

        // $cart[$product->id] = $quantity;
        // return redirect()->back()->withCookie(cookie()->forever('cart', $cart));
    }
    public function index()
    {
        if (Auth::guest()) {
            Alert::warning('Please login first to see your item in the basket', 'Sorry');
            return redirect()->back();
        } else {
            $cart = DB::table('cart')
                ->join('products', 'products.id', '=', 'cart.product_id')
                ->join('users', 'cart.user_id', '=', 'users.id')
                ->select('cart.*', 'products.name as product_name', 'products.price', 'products.model', 'products.photo')
                ->where('user_id', Auth::user()->id)
                ->get();
            // dd($cart);

            $price = DB::table('cart')
                ->join('products', 'products.id', '=', 'cart.product_id')
                ->join('users', 'cart.user_id', '=', 'users.id')
                ->select(DB::raw('SUM(products.price*cart.quantity) as total'))
                ->where('user_id', Auth::user()->id)
                ->first();

            // dd($price);
            return view('frontend.cart.index', compact('cart', 'price'));
        }
    }

    public function delete($id)
    {
        $cart = Cart::findOrFail($id);
        $cart->delete();
        Alert::success('Data successfully deleted', 'Good Job')->autoclose(1500);
        return view('frontend.cart.index', compact('cart'));
    }

    public function updateCart(Cart $cart, Request $request)
    {
        $cart = Cart::Find($cart->slug);
        dd($cart);
    }
}
