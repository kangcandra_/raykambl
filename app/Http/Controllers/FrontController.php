<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Client;
use App\Models\ClientCategory;
use App\Models\Portofolio;
use App\Models\PortofolioCategory;
use App\Models\Post;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductTag;
use App\Models\Tag;
use App\Models\Team;
use App\Models\Testimonial;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')->take(3)->get();
        $portofolio = Portofolio::orderBy('created_at', 'desc')->take(3)->get();
        $testimonial = Testimonial::orderBy('created_at', 'desc')->take(3)->get();
        $client = Client::orderBy('created_at', 'desc')->take(4)->get();
        // $posts = Post::search('think')->get();
        // dd($posts);
        return view('frontend.index', compact('posts', 'portofolio', 'testimonial', 'client'));
    }

    public function about()
    {
        $team = Team::orderBy('created_at', 'asc')->take(12)->get();
        $client = Client::orderBy('created_at', 'desc')->take(4)->get();
        $testimonial = Testimonial::orderBy('created_at', 'desc')->take(2)->get();
        return view('frontend.about', compact('testimonial', 'client', 'team'));
    }

    public function product()
    {
        $category = ProductCategory::all();
        $product = Product::orderBy('created_at', 'desc')->take(9)->paginate();
        return view('frontend.product.index   ', compact('product', 'category'));
    }

    public function singleproduct(Product $product)
    {
        // $product = Post::where('slug', $slug)->first();
        $pr = Product::orderBy('created_at', 'desc')->take(3)->paginate();
        $tags = ProductTag::all();
        // dd($product);
        return view('frontend.product.show', compact('product', 'pr', 'tags'));
    }

    public function blog()
    {
        $blog = Post::orderBy('created_at', 'desc')->paginate(1);
        // dd(get_class($blog));
        return view('frontend.blog.index', compact('blog'));
    }

    public function blogtag(Tag $tag)
    {
        $blog = $tag->Post()->latest()->paginate(5);
        return view('frontend.blog.index', compact('blog'));
    }

    public function blogcategory(Category $category)
    {
        $blog = $category->Post()->latest()->paginate(5);
        return view('frontend.blog.index', compact('blog'));
    }

    public function singleblog(Post $blog)
    {
        // $blog = Post::where('slug', $slug)->first();
        // $blog->viewers += 1;
        // $blog->save();
        $next = Post::where('id', $blog->id + 1)->pluck('slug')->first();
        $previous = Post::where('id', $blog->id - 1)->pluck('slug')->first();
        $tags = Tag::all();
        // dd($blog);
        return view('frontend.blog.show', compact('blog', 'next', 'previous', 'tags'));
    }

    public function testimonial()
    {
        $testimonial = Testimonial::latest()->paginate(6);
        return view('frontend.testimonial', compact('testimonial'));
    }

    public function portofolio()
    {
        $category = PortofolioCategory::all();
        $portofolio = Portofolio::latest()->paginate(6);
        return view('frontend.portofolio.index', compact('portofolio', 'category'));
    }

    public function client()
    {
        $category = ClientCategory::all();
        $client = Client::latest()->paginate(6);
        return view('frontend.client', compact('client', 'category'));
    }

    //Product
    public function cart(Request $request)
    {
        $cart = $request->all();
        // dd($cart);
        return view('frontend.product.cart', compact('cart'));
    }

    public function service()
    {
        return view('frontend.service');
    }
}
