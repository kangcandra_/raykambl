<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Product;

class ProductController extends Controller
{
    public function index()
    {
        $product = Product::all();
        $response = ["message" => "berhasil",
            "data" => $product];
        return response()->json($response, 200);
    }
}
