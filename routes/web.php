<?php

use App\Http\Controllers\Backend\AdminController;
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\ClientController;
use App\Http\Controllers\Backend\PortofolioCategoryController;
// use App\Http\Controllers\ContactController;
use App\Http\Controllers\Backend\PortofolioController;
use App\Http\Controllers\Backend\PostController;
use App\Http\Controllers\Backend\ProductCategoryController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\ProductTagController;
use App\Http\Controllers\Backend\SettingsController;
use App\Http\Controllers\Backend\TagController;
use App\Http\Controllers\Backend\TeamController;
use App\Http\Controllers\Backend\TestimonialController;
use App\Http\Controllers\Backend\UsersController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\RssController;
use App\Http\Controllers\SearchesController;

Route::get('/home', [HomeController::class,'index']);
Auth::routes();
//backend admin
Route::group(['prefix' => 'admin', 'as' => 'backend.', 'middleware' => ['auth']], function () {
    Route::get('/', [AdminController::class, 'index']);
    Route::resource('users', UsersController::class);
    // Route::resource('maps', );
    Route::resource('post', PostController::class);
    Route::resource('post_categories', CategoryController::class);
    Route::resource('post_tags', TagController::class);
    Route::resource('portofolio_categories', PortofolioCategoryController::class);
    Route::resource('portofolio', PortofolioController::class);
    Route::resource('product_categories', ProductCategoryController::class);
    // Route::match(['get','post'],'product-attribute/{id}','Backend\ProductController@addAttribute');
    Route::resource('product', ProductController::class);
    Route::resource('product_tag', ProductTagController::class);
    Route::resource('testimonial', TestimonialController::class);
    Route::resource('client_categories', ClientCategoryController::class);
    Route::resource('client', ClientController::class);
    Route::resource('team', TeamController::class);
    Route::resource('team_division', DivisionController::class);
    Route::get(
        '/settings',
        array('as' => 'settings.index', 'uses' => [SettingsController::class, 'profile'])
    );
    Route::post(
        '/settings',
        array('as' => 'settings.profile',
            'uses' => [SettingsController::class, 'UpdateProfile'])
    );
});

//frontend
Route::group(['prefix' => '/', 'as' => 'front.'], function () {
    Route::get('/', [FrontController::class, 'index']);
    Route::get('blog', [FrontController::class, 'blog']);
    Route::get('blog/tag/{tag}', array('as' => 'blogtag', 'uses' => [FrontController::class, 'blogtag']));
    Route::get('blog/category/{category}', array('as' => 'blogcategory', 'uses' => [FrontController::class, 'blogcategory']));
    Route::get('blog/{blog}', array('as' => 'singleblog', 'uses' => [FrontController::class, 'singleblog']));
    // Route::resource('contact', 'ContactController');
    Route::resource('contact', ContactController::class);
    Route::get('about', [FrontController::class, 'about']);
    Route::get('product', [FrontController::class, 'product']);
    Route::get('product/{product}', [FrontController::class, 'singleproduct'])->name('singleproduct');
    Route::get('testimonial', [FrontController::class, 'testimonial']);
    Route::get('portofolio', [FrontController::class, 'portofolio']);
    Route::get('client', [FrontController::class, 'client']);
    Route::get('service', [FrontController::class, 'service']);
    Route::get('search', [SearchesController::class, 'search']);
    Route::get('/rss', [RssController::class, 'rss']);
    // cart
    Route::get('cart', [CartController::class, 'index']);
    Route::post('cart', [
        'as' => 'cart',
        'uses' => [CartController::class, 'addTocart'],
    ]);
    Route::delete('cart/{id}', [
        'as' => 'cart.delete',
        'uses' => [CartController::class, 'delete'],
    ]);

    Route::get('cart/update/{cart}', [
        'as' => 'cart.update',
        'uses' => [CartController::class, 'updateCart'],
    ]);

    Route::get('checkout', [
        'as' => 'checkout',
        'uses' => [CartController::class, 'checkout'],
    ]);
});
