<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('secret'),
            'permission' => 'Admin'
        ]);


        User::create([
            'name' => 'Author Assalaam',
            'email' => 'author@gmail.com',
            'password' => bcrypt('secret'),
            'permission' => 'Author'
        ]);

        User::create([
            'name' => 'Candra Herdiansyah',
            'email' => 'candraherdiansyah14@gmail.com',
            'password' => bcrypt('candra'),
            'permission' => 'Admin'
        ]);
    }
}
