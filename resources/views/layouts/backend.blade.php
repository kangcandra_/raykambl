<html lang="en">

<head>
    <meta charset="utf-8">
    <title>{{ config('app.name', 'Laravel') }} || Admin</title>
    <link rel="stylesheet" href="">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
        type="text/css">
    <link rel="stylesheet" href="{{ asset('/assets/backend/global/plugins/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('/assets/backend/global/plugins/simple-line-icons/simple-line-icons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/backend/global/plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('/assets/backend/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('/assets/backend/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/backend/global/plugins/morris/morris.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/backend/global/plugins/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/backend/global/plugins/jqvmap/jqvmap/jqvmap.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/backend/global/css/components-md.min.css') }}"
        id="style_components">
    <link rel="stylesheet" href="{{ asset('/assets/backend/global/css/plugins-md.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/backend/layouts/layout4/css/layout.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/backend/layouts/layout4/css/themes/default.min.css') }}" id="
        style_color">
    <link rel="stylesheet" href="{{ asset('/assets/backend/layouts/layout4/css/custom.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/backend/pages/css/sweetalert.css') }}">
    @yield('css')
    <link rel="shortcut icon" href="{{ asset('/assets/backend/logo1.png') }}">
</head>

<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
    @include('partials.backend.navbar')
    <div class="clearfix"> </div>
    <div class="page-container">
        @include('partials.backend.sidebar')
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-head">
                    <div class="page-title">
                        <h1>@yield('title')
                            <small>@yield('subtitle')</small>
                        </h1>
                    </div>
                </div>
                <div class="row">
                    @include('layouts._flash')
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    <div class="page-footer">
        <div class="page-footer-inner"> &copy; Assalaam Studio {{ date('Y') }}
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <div class="quick-nav-overlay"></div>
    <script src=""></script>
    <script src="{{ asset('/assets/backend/global/plugins/jquery.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/global/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/global/plugins/js.cookie.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/global/plugins/jquery.blockui.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/global/plugins/moment.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}">
    </script>
    <script src="{{ asset('/assets/backend/global/plugins/morris/morris.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/global/plugins/morris/raphael-min.js') }}"></script>
    <script src="{{ asset('/assets/backend/global/plugins/counterup/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/global/plugins/counterup/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/global/plugins/horizontal-timeline/horizontal-timeline.js') }}"></script>
    <script src="{{ asset('/assets/backend/global/plugins/flot/jquery.flot.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/global/plugins/flot/jquery.flot.resize.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/global/plugins/flot/jquery.flot.categories.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/global/plugins/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/global/scripts/app.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/pages/scripts/dashboard.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/layouts/layout4/scripts/layout.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/layouts/layout4/scripts/demo.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/layouts/global/scripts/quick-sidebar.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/layouts/global/scripts/quick-nav.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/pages/scripts/sweetalert.min.js') }}"></script>
    <script src="{{ asset('/assets/backend/pages/scripts/scripts.js') }}"></script>
    @include('sweetalert::alert')
    <script>
        $(document).ready(function() {
            $('#clickmewow').click(function() {
                $('#radio1003').attr('checked', 'checked');
            });
        })
    </script>
    @yield('js')
    @stack('scripts')
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
    </script>
</body>

</html>
