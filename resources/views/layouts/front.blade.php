<!DOCTYPE html>
<html lang="en">

<head lang="{{ app()->getLocale() }}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="icon" type="image/png" href="{{ asset('assets/front/images/logo1.png') }}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{ asset('assets/front/images/logo1.png') }}" sizes="16x16" />
    {!! Minify::stylesheet(['/assets/front/css/fonts.css']) !!}
    {!! Minify::stylesheet(['/assets/front/css/crumina-fonts.css']) !!}
    {!! Minify::stylesheet(['/assets/front/css/normalize.css']) !!}
    {!! Minify::stylesheet(['/assets/front/css/grid.css']) !!}
    {!! Minify::stylesheet(['/assets/front/css/base.css']) !!}
    {!! Minify::stylesheet(['/assets/front/css/blocks.css']) !!}
    {!! Minify::stylesheet(['/assets/front/css/layouts.css']) !!}
    {!! Minify::stylesheet(['/assets/front/css/modules.css']) !!}
    {!! Minify::stylesheet(['/assets/front/css/widgets-styles.css']) !!}


    <!--Plugins styles-->

    {!! Minify::stylesheet(['/assets/front/css/jquery.mCustomScrollbar.min.css']) !!}
    {!! Minify::stylesheet(['/assets/front/css/swiper.min.css']) !!}
    {!! Minify::stylesheet(['/assets/front/css/primary-menu.css']) !!}
    {!! Minify::stylesheet(['/assets/front/css/magnific-popup.css']) !!}
    {!! Minify::stylesheet(['/assets/backend/pages/css/sweetalert.css']) !!}
    @yield('css')
    @yield('share')
    <!--Styles for RTL-->

    {{-- <!--{!! Minify::stylesheet(array('assets/front/css/rtl.css')) !!}--> --}}

    <!--External fonts-->

    <link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>

</head>


<body class=" ">

    <!-- Header -->


    <header class="header" id="site-header">

        <div class="container">
            <div class="header-content-wrapper">
                <div class="logo">
                    <a href="{{ url('/') }}" class="full-block-link"></a>
                    <img src="{{ asset('assets/front/images/logo.png') }}" alt="Seosight"
                        style="width: 220px; height: 52px;">
                    <div class="logo-text">
                    </div>
                </div>
                {{-- Nav --}}
                @include('partials.frontend.nav')
                {{-- End Nav --}}



                @guest
                    <div class="user-menu open-overlay">
                        <a href="#" class="user-menu-content  js-open-aside">
                            <span></span>
                            <span></span>
                            <span></span>
                        </a>
                    </div>
                @endguest
            </div>

        </div>

    </header>
    <!-- ... End Header -->

    {{-- Right Menu --}}
    @include('partials.frontend.login')
    {{-- End Right Menu --}}
    <div class="content-wrapper">
        <div class="header-spacer"></div>
        {{-- content --}}
        @yield('content')
        {{-- end content --}}

        <!-- Subscribe Form -->
        {{-- @include('partials.frontend.subscribe') --}}
        <!-- End Subscribe Form -->
    </div>



    <!-- Footer -->

    @include('partials.frontend.footer')

    <!-- End Footer -->

    {{-- svg --}}
    @include('partials.frontend.svg')
    {{-- end svg --}}

    <!-- Overlay Search -->

    <div class="overlay_search">
        <div class="container">
            <div class="row">
                <div class="form_search-wrap">
                    <form action="{{ url('search') }}" method="GET">
                        <input class="overlay_search-input" name="q" placeholder="Type and hit Enter..." type="text">
                        <a href="#" class="overlay_search-close">
                            <span></span>
                            <span></span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- End Overlay Search -->

    <!-- JS Script -->

    {!! Minify::javascript('/assets/front/js/jquery-2.1.4.min.js') !!}
    {!! Minify::javascript('/assets/front/js/crum-mega-menu.js') !!}
    {!! Minify::javascript('/assets/front/js/swiper.jquery.min.js') !!}
    {!! Minify::javascript('/assets/front/js/theme-plugins.js') !!}
    {!! Minify::javascript('/assets/front/js/main.js') !!}
    {!! Minify::javascript('/assets/front/js/form-actions.js') !!}

    {!! Minify::javascript('/assets/front/js/velocity.min.js') !!}
    {!! Minify::javascript('/assets/front/js/ScrollMagic.min.js') !!}
    {!! Minify::javascript('/assets/front/js/animation.velocity.min.js') !!}
    {!! Minify::javascript('/assets/backend/pages/scripts/sweetalert.min.js') !!}
    <script src='https://www.google.com/recaptcha/api.js'></script>
    @include('sweetalert::alert')
    @yield('js')
    @stack('scripts')
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/59b8d85cc28eca75e461fbf8/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>

    <!-- ...end JS Script -->

</body>

</html>
