@extends('layouts.front')
@section('css')
    {{-- <style>
    #map{
        width: 100%;
        height: 600px;
    }
    </style> --}}
@endsection
@section('js')
    {{-- <script type="text/javascript">
        var map;
        $(document).ready(function(){
        map = new GMaps({
            el: '#map',
            lat: -6.965917,
            lng: 107.592911,
            zoom: 17,
        });
        map.addMarker({
            lat: -6.965917,
            lng: 107.592911,
            title: 'SMK Assalaam Bandung | Assalaam Studio',
            infoWindow: {
                content: '<p>SMK Assalaam Bandung | Assalaam Studio</p>'
            }
        });
        });
    </script> --}}
{{-- {!! Minify::javascript('/assets/front/js/gmaps.js')!!} --}}
{{-- <script src="https://maps.googleapis.com/maps/api/js"></script> --}}

{{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4peMLkpr4PIxRjj2knCE-qB93zP38YWk&amp;libraries=places"></script> --}}
@endsection
@section('content')
<!-- Stunning header -->

<div class="stunning-header stunning-header-bg-blue">
    <div class="stunning-header-content">
        <h1 class="stunning-header-title">Contact Information</h1>
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a href="{{ url('/') }}">Home</a>
                <i class="seoicon-right-arrow"></i>
            </li>
            <li class="breadcrumbs-item active">
                <span href="#">Contact Information</span>
                <i class="seoicon-right-arrow"></i>
            </li>
        </ul>
    </div>
</div>
<!-- End Stunning header -->
<!-- Contacts -->


<div class="container-fluid">
    <div class="row medium-padding80 bg-border-color contacts-shadow" style="padding: 50px;">
        <div class="container">
            <div class="row">
                <div class="contacts">
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <div class="contacts-item">
                            <img src="{{ asset('/assets/front/img/contact7.png') }}" alt="phone">
                            <div class="content">
                                <a href="#" class="title">Bandung, Indonesia</a>
                                <p class="sub-title">Jl. Situ Tarate, Cibaduyut 40265</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <div class="contacts-item">
                            <img src="{{ asset('/assets/front/img/contact8.png') }}" alt="phone">
                            <div class="content">
                                <a href="#" class="title">info@assalamstudio.com</a>
                                <p class="sub-title">online support</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <div class="contacts-item">
                            <img src="{{ asset('/assets/front/img/contact9.png') }}" alt="phone">
                            <div class="content">
                                <a href="#" class="title">(022) 5420220</a>
                                <p class="sub-title">Mon-Sat 8am-6pm</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Contacts -->
<!-- Google map -->


<div class="section">
     {{-- <div id="map"></div>  --}}
     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.3674102673267!2d107.59072401424449!3d-6.965911270139242!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e8deccecb6f1%3A0x658cc60fbe5017b9!2sSMK+Assalaam+Bandung!5e0!3m2!1sen!2sid!4v1534399974948" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<!-- End Google map -->
<!-- Contact form -->
<div class="container-fluid" style="background-color : #2c3e50; width:auto    ">
    <div class="contact-form medium-padding" style="padding:80px;">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="heading">
                    <h4 class="heading-title" style="color: #fff">Have You Any Questions?</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>
                    <p class="heading-text">Please contact us using the form and we’ll get back to you as soon as possible.</p>
                </div>
            </div>
        </div>
        {!! Form::open(array('route' => 'front.contact.store', 'class' => 'contact-form')) !!}    
        <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('name') ? ' has-error' : '' }}">
                    <input name="name" class="email input-standard-grey" placeholder="Your Name" type="text" required>
                     @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>

            </div>

            <div class="row">

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input name="email" class="email input-standard-grey" placeholder="Email Address" type="email" required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12{{ $errors->has('phone') ? ' has-error' : '' }}">
                    <input name="phone" class="email input-standard-grey" placeholder="Phone" type="text">
                    @if ($errors->has('phone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                </div>

            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('pesan') ? ' has-error' : '' }}">
                    <textarea name="pesan" class="email input-standard-grey" placeholder="Details"></textarea>
                    @if ($errors->has('pesan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('pesan') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('pesan') ? ' has-error' : '' }}">
                   {!! app('captcha')->display() !!}
                   {!! $errors->first('g-recaptcha-response', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <br>
            <div class="row">
                <div class="submit-block table">
                    <div class="col-lg-3 table-cell">
                        <button class="btn btn-small btn--primary">
                            <span class="text">Contact Us!</span>
                        </button>
                    </div>

                  <div class="col-lg-5 table-cell">
                        <div class="submit-block-text">
                            Please, let us know any particular things to check and the best time
                            to contact you by phone (if provided).
                        </div>
                  </div>
                </div>
            </div>
            {!! Form::close() !!}
    </div>
</div>

<!-- End Contact form -->
@endsection