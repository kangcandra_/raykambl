@extends('layouts.front')
@section('share')
<!-- for Google -->
<meta name="description" content="{!! str_limit(strip_tags($blog->content, 70) )!!}" />
<meta name="keywords" content="{!! str_limit(strip_tags($blog->content, 70) )!!}" />
<meta name="author" content="{{ config('app.name', 'Laravel') }}" />
<meta name="copyright" content="{{ config('app.name', 'Laravel') }}" />
<meta name="application-name" content="" />

<!-- for Facebook -->
<meta property="og:title" content="{{ $blog->title }}" />
<meta property="og:type" content="blog" />
<meta property="og:image" content="{{ asset('/assets/img/post/'.$blog->photo) }}" />
<meta property="og:url" content="{{ url('/blog/' .$blog->slug) }}" />
<meta property="og:description" content="{!! str_limit(strip_tags($blog->content, 70))!!}" />

<!-- for Twitter -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="">
<meta name="twitter:creator" content="">
<meta name="twitter:title" content="{{ $blog->title }}">
<meta name="twitter:description" content="{!! str_limit(strip_tags($blog->content, 70)) !!}">
<meta name="twitter:creator" content="@smkassalaam">
<meta name="twitter:image" content="{{ asset('/assets/img/post/'.$blog->photo) }}">

<meta name="_token" content="{!! csrf_token() !!}" />
@endsection
@section('content')
<div class="stunning-header stunning-header-bg-lightviolet">
	<div class="stunning-header-content">
		<h1 class="stunning-header-title">{{ $blog->title }}</h1>
		{{-- <ul class="breadcrumbs">
			<li class="breadcrumbs-item">
				<a href="index-2.html">Home</a>
				<i class="seoicon-right-arrow"></i>
			</li>
			<li class="breadcrumbs-item active">
				<span href="#">Blog Details</span>
				<i class="seoicon-right-arrow"></i>
			</li>
		</ul> --}}
	</div>
</div>
<div class="container">
	<div class="row medium-padding120" style="padding: 20px;">
		<main class="main">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<article class="hentry post post-standard-details" style="margin-bottom: 20px;">

					<div class="post-thumb">
						<img src="{{ asset('/assets/img/post/'.$blog->photo.'') }}" alt="seo" style="width: 750px; height: 470px;">
					</div>

					<div class="post__content">

						<h2 class="h2 post__title entry-title ">
							<a href="#">{{-- {{ $blog->title }} --}}</a>
						</h2>


						<div class="post-additional-info">

							<div class="post__author author vcard">
								<img src="/assets/front/img/avatar-b-details.png" alt="author">
								Posted by 

								<div class="post__author-name fn">
									<a href="#" class="post__author-link"> {{ $blog->User->name }}</a>
								</div>

							</div>

							<span class="post__date">

								<i class="seoicon-clock"></i>

								<time class="published" datetime="2016-03-20 12:00:00">
									{{$blog->created_at->format(' d M Y ') }}
								</time>

							</span>

							<span class="category">
								<i class="seoicon-tags"></i>
								<a href="/blog/category/{{$blog->Category->slug}}">{{$blog->Category->name}}</a>
							</span>

						</div>

						<div class="post__content-info">

							{{-- <p class="post__subtitle">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
								euismod tincidunt ut laoreet dolore.
							</p> --}}

							<p class="post__text">{!! $blog->content !!}
							</p>


							<div class="widget w-tags">
								<div class="tags-wrap">
									@foreach($blog->Tag as $data)
									<a href="/blog/tag/{{ $data->slug }}" class="w-tags-item">{{ $data->name }}</a>
									@endforeach
								</div>
							</div>

						</div>
					</div>

					<div class="socials">Share:
						<a target="_blank" onclick="return !window.open(this.href, 'Facebook', 'width=800,height=500')" href="http://www.facebook.com/sharer.php?u={{ url('/blog/' .$blog->slug) }}" class="social__item facebook">
							<i class="seoicon-social-facebook"></i>
						</a>
						<a  target="_blank" onclick="return !window.open(this.href, 'Twitter', 'width=500,height=300')" href="https://twitter.com/intent/tweet?text={{ $blog->title }}&url={{ url('blog/' .$blog->slug) }}" class="social__item">
							<i class="seoicon-social-twitter"></i>
						</a>
						{{-- <a href="#" class="social__item">
							<i class="seoicon-social-linkedin"></i>
						</a> --}}
						<a href="https://plus.google.com/share?url={{ url('blog/' .$blog->slug) }}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="social__item">
							<i class="seoicon-social-google-plus"></i>
						</a>
						{{-- <a href="#" class="social__item">
							<i class="seoicon-social-pinterest"></i>
						</a> --}}
					</div>

				</article>

				<div class="blog-details-author">

					<div class="blog-details-author-thumb">
						<img src="/assets/front/img/blog-details-author.png" alt="Author">
					</div>

					<div class="blog-details-author-content">
						<div class="author-info">
							<h5 class="author-name">{{ $blog->User->name }}</h5>
							<p class="author-info">{{ $blog->User->permission }}</p>
						</div>
						<p class="text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
							nonummy nibh euismod.
						</p>
						<div class="socials">

							<a href="#" class="social__item">
								<img src="/assets/front/svg/circle-facebook.svg" alt="facebook">
							</a>

							<a href="#" class="social__item">
								<img src="/assets/front/svg/twitter.svg" alt="twitter">
							</a>

							<a href="#" class="social__item">
								<img src="/assets/front/svg/google.svg" alt="google">
							</a>

							<a href="#" class="social__item">
								<img src="/assets/front/svg/youtube.svg" alt="youtube">
							</a>

						</div>
					</div>
				</div>
				<div class="pagination-arrow" style="padding: 15px; margin-bottom: 20px;">
					@if($previous)
					<a href="{{ URL::to( 'blog/' . $previous) }}" class="btn-prev-wrap">
						<svg class="btn-prev">
							<use xlink:href="#arrow-left"></use>
						</svg>
						<div class="btn-content">
							<div class="btn-content-title"> Previous Post</div>
							{{-- <p class="btn-content-subtitle">{{ $blog->title }}</p> --}}
						</div>
					</a>
					@endif
					@if($next)
					<a href="{{ URL::to( 'blog/' . $next) }}" class="btn-next-wrap">
						<div class="btn-content">
							<div class="btn-content-title">Next Post</div>
							{{-- <p class="btn-content-subtitle">{{ $blog->title }}</p> --}}
						</div>
						<svg class="btn-next">
							<use xlink:href="#arrow-right"></use>
						</svg>
					</a>
					@endif
				</div>


				{{-- <div class="comments">
				</div> --}}

				<div class="row">
					<div class="leave-reply contact-form"  id="disqus_thread">
					</div>
				</div>
			</div>
			<!-- End Post Details -->

			<!-- Sidebar-->
			@include('frontend.blog.partials.sidebar')
			<!-- End Sidebar-->

		</main>
	</div>
</div>
@endsection
@push('scripts')
<script>
var disqus_config = function () {
this.page.url = '{{ url('blog/' .$blog->slug) }}'; 
this.page.identifier = '{{ $blog->id }}'; 
};

(function() {
var d = document, s = d.createElement('script');
s.src = 'https://assalaamstudio.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<script id="dsq-count-scr" src="//assalaamstudio.disqus.com/count.js" async></script>
@endpush