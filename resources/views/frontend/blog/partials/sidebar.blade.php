 {{-- Sidebar --}}

<div class="col-lg-3 col-lg-offset-1 col-md-4 col-sm-12 col-xs-12">
	<aside aria-label="sidebar" class="sidebar sidebar-right">
		<div class="widget">
			<form class="w-search" action="{{ url('search') }}" method="GET">
				<input class="email search input-standard-grey" name="q" required="required" placeholder="Search" type="search">
				<button class="icon">
					<i class="seoicon-loupe"></i>
				</button>
			</form>
		</div>

		<div class="widget w-post-category">
			<div class="heading">
				<h4 class="heading-title">Post Category</h4>
				<div class="heading-line">
					<span class="short-line"></span>
					<span class="long-line"></span>
				</div>
			</div>
			<div class="post-category-wrap">
				@foreach($category as $data)
				@if($data->Post->count() > 0)
				<div class="category-post-item">
					<span class="post-count">{{ $data->Post->count() }}</span>
					<a href="/blog/category/{{ $data->slug }}" class="category-title">{{ $data->name }}
						<i class="seoicon-right-arrow"></i>
					</a>
				</div>
				@endif
				@endforeach
			</div>
		</div>

		<div class="widget w-about">
			<div class="heading">
				<h4 class="heading-title">About Us and
					This Blog</h4>
				<div class="heading-line">
					<span class="short-line"></span>
					<span class="long-line"></span>
				</div>
			</div>
			<p>SMK Assalaam adalah bagian dari Yayasan Assalaam yang mempersiapkan siswa untuk siap kerja dengan keterampilan & profesional di bidang industri (sekolah berbasis industri) dan kewirausahaan.</p>
			<a href="{{ url('/about') }}" class="btn btn-small btn-border c-primary">
				<span class="text">Learn More</span>
				<i class="seoicon-right-arrow"></i>
			</a>
		</div>
		<div class="widget w-request bg-boxed-red">
			<div class="w-request-content">
				<img src="/assets/front/img/request.png" alt="request">
				<h4 class="w-request-content-title">Request
					a Free Quote</h4>
				<p class="w-request-content-text">Gothica, quam nunc putamus parum claram, anteposuerit
					litterarum formas humanitatis.
				</p>

				<a href="{{ url('/contact') }}" class="btn btn-small btn--dark btn-hover-shadow">
					<span class="text">Contact Now</span>
				</a>
			</div>
		</div>

		<div class="widget w-latest-news">
			<div class="heading">
				<h4 class="heading-title">Latest News</h4>
				<div class="heading-line">
					<span class="short-line"></span>
					<span class="long-line"></span>
				</div>
			</div>
			@foreach($blogdata as $data)
			<div class="latest-news-wrap">
				<div class="latest-news-item">
					<div class="post-additional-info">

						<span class="post__date">
							<i class="seoicon-clock"></i>
							<time class="published" datetime="2016-04-23 12:00:00">
								{{$data->created_at->format('d, M Y') }}
							</time>
						</span>
					</div>
					<h5 class="post__title entry-title ">
						<a href="/blog/{{ $data->slug }}">{{ $data->title }}</a>
					</h5>
				</div>
			</div>
			@endforeach

			<a href="{{ url('/blog') }}" class="btn btn-small btn--dark btn-hover-shadow">
				<span class="text">All News</span>
				<i class="seoicon-right-arrow"></i>
			</a>
		</div>

		<div class="widget w-follow">
			<div class="heading">
				<h4 class="heading-title">Follow Us</h4>
				<div class="heading-line">
					<span class="short-line"></span>
					<span class="long-line"></span>
				</div>
			</div>

			<div class="w-follow-wrap">
				<div class="w-follow-item facebook-bg-color">
					<a href="#" class="w-follow-social__item table-cell">
						<i class="seoicon-social-facebook"></i>
					</a>
					<a href="#" class="w-follow-title table-cell">Facebook
						<span class="w-follow-add">
							<i class="seoicon-cross plus"></i>
							<i class="seoicon-check-bold check"></i>
						</span>
					</a>
				</div>
				<div class="w-follow-item twitter-bg-color">
					<a href="#" class="w-follow-social__item table-cell">
						<i class=" seoicon-social-twitter"></i>
					</a>
					<a href="#" class="w-follow-title table-cell">Twitter
						<span class="w-follow-add active">
							<i class="seoicon-cross plus"></i>
							<i class="seoicon-check-bold check"></i>
						</span>
					</a>
				</div>
				<div class="w-follow-item linkedin-bg-color">
					<a href="#" class="w-follow-social__item table-cell">
						<i class="seoicon-social-linkedin"></i>
					</a>
					<a href="#" class="w-follow-title table-cell">Linkedin
						<span class="w-follow-add">
							<i class="seoicon-cross plus"></i>
							<i class="seoicon-check-bold check"></i>
						</span>
					</a>
				</div>
				<div class="w-follow-item google-bg-color">
					<a href="#" class="w-follow-social__item table-cell">
						<i class="seoicon-social-google-plus"></i>
					</a>
					<a href="#" class="w-follow-title table-cell">Google+
						<span class="w-follow-add">
							<i class="seoicon-cross plus"></i>
							<i class="seoicon-check-bold check"></i>
						</span>
					</a>
				</div>
				<div class="w-follow-item pinterest-bg-color">
					<a href="#" class="w-follow-social__item table-cell">
						<i class="seoicon-social-pinterest"></i>
					</a>
					<a href="#" class="w-follow-title table-cell">Pinterest
						<span class="w-follow-add">
							<i class="seoicon-cross plus"></i>
							<i class="seoicon-check-bold check"></i>
						</span>
					</a>
				</div>
			</div>

		</div>

		<div  class="widget w-tags">
			<div class="heading">
				<h4 class="heading-title">Popular Tags</h4>
				<div class="heading-line">
					<span class="short-line"></span>
					<span class="long-line"></span>
				</div>
			</div>

			<div class="tags-wrap">
				@foreach($tags as $data)
				@if($data->Post->count() > 0)
				<a href="/blog/tag/{{ $data->slug }}" class="w-tags-item">{{ $data->name }}</a>
				@endif
				@endforeach
			</div>
		</div>
	</aside>
</div>

 {{-- End Sidebar  --}}