@extends('layouts.front')
@section('js')
{!! Minify::javascript('/assets/front/js/isotope.pkgd.min.js')!!}
@endsection
@section('content')
{{-- @include('frontend.partials.slider') --}}
<!-- Stunning header -->
<div class="stunning-header stunning-header-bg-breez">
    <div class="stunning-header-content">
        <h1 class="stunning-header-title">Product</h1>
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a href="{{ url('/') }}">Home</a>
                <i class="seoicon-right-arrow"></i>
            </li>
            <li class="breadcrumbs-item active">
                <span href="#">Product</span>
                <i class="seoicon-right-arrow"></i>
            </li>
        </ul>
    </div>
</div>
<!-- End Stunning header -->
<!-- Case Item -->

<div class="container">
    <div class="row medium-padding" style="padding: 20px;">

        <div class="col-lg-12">

            <div class="heading align-center">
                <h4 class="h1 heading-title">Our Work</h4>
                <div class="heading-line">
                    <span class="short-line"></span>
                    <span class="long-line"></span>
                </div>
                <p class="heading-text">Kami mencintai apa yang kami lakukan. Menjadi menyenangkan & ramah adalah cara kami berkomunikasi, dan kami menaruh hati dalam setiap pekerjaan yang kami lakukan.
                </p>
            </div>

            <ul class="cat-list align-center sorting-menu">
                <li class="cat-list__item active" data-filter="*"><a href="#" class="">All Projects</a></li>
                @foreach($category as $data)
                @if($data->Portofolio->count() > 0)
                <li class="cat-list__item" data-filter=".portofolio{{ $data->id }}"><a href="#" class="">{{ $data->name }}</a></li>
                @endif
                @endforeach
            </ul>


            <div class="row sorting-container" data-layout="fitRows">
                <div class="grid-sizer col-lg-4 col-md-4"></div>
                @foreach($portofolio as $data)
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 sorting-item portofolio{{ $data->PortofolioCategory->id }}">
                    <div class="case-item align-center big mb60">
                        <div class="case-item__thumb mouseover lightbox shadow animation-disabled">
                            <img src="{{ asset('/assets/img/portofolio/'.$data->photo.'') }}"  style="width: 496px; height: 372px;" alt="our case">
                        </div>
                        <h5 class="case-item__title">{{ $data->title }}</h5>
                        <div class="case-item__cat">
                            <a href="#">{{ $data->PortofolioCategory->name }}</a>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>

            <div class="row">
            {{ $portofolio->links('vendor.pagination.custom') }}

            </div>

        </div>

    </div>
</div>

<!-- End Case Item -->
@endsection