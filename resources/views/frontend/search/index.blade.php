@extends('layouts.front')
@section('content')
@include('frontend.partials.slider')

<!-- Blog posts-->
<div class="container">
	<div class="row medium" style="padding: 20px;">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
			<main class="main">
				@foreach($posts as $data)
				<article class="hentry post post-standard has-post-thumbnail thumbnail">
					<div class="post-thumb">
						<img src="{{ asset('/assets/img/post/'.$data->photo.'') }}" alt="seo" style="width: 690px; height: 418px">
						<div class="overlay"></div>
						<a href="{{ asset('/assets/img/post/'.$data->photo.'') }}" class="link-image js-zoom-image">
							<i class="seoicon-zoom"></i>
						</a>
						<a href="#" class="link-post">
							<i class="seoicon-link-bold"></i>
						</a>
					</div>

					<div class="post__content">

						 <div class="post__author author vcard">
							 <img src="/assets/front/img/avatar6.png" alt="author">
							 Posted by

							 <div class="post__author-name fn">
							 	<a href="#" class="post__author-link">{{ $data->User->name }}</a>
							 </div>

						 </div>

						<div class="post__content-info">

								<h2 class="post__title entry-title ">
									<a href="/blog/{{$data->slug}}">{{ str_limit($data->title,30,'') }}</a>
								</h2>

								<div class="post-additional-info">

									<span class="post__date">

										<i class="seoicon-clock"></i>

										<time class="published" datetime="2016-04-17 12:00:00">
											{{$data->created_at->format(' d M Y ') }}
										</time>

									</span>

									<span class="category">
										<i class="seoicon-tags"></i>
										<a href="/blog/category/{{$data->Category->slug}}">{{$data->Category->name}}</a>
									</span>

								</div>

									<p class="post__text">{{-- {!! $data->content !!} --}}
									</p>

								<a href="{{ URL::to( '/blog/' . $data->slug) }}" class="btn btn-small btn--dark btn-hover-shadow">
									<span class="text">Continue Reading</span>
									<i class="seoicon-right-arrow"></i>
								</a>
						</div>
					</div>
				</article>
				@endforeach
				{{-- {{ $posts->links('vendor.pagination.custom') }} --}}
			</main>
		</div>


		<!-- Sidebar-->
		@include('frontend.blog.partials.sidebar')
		<!-- End Sidebar-->


	</div>
</div>
<!-- End Blog posts-->
@endsection