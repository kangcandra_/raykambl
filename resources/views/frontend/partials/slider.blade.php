@section('slider')
<div class="container-full-width">
    <div class="swiper-container main-slider main-slider-bg-photo-wrap" data-effect="fade" data-autoplay="3000">

        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->
            @foreach($post as $data)
            <div class="main-slider-bg-photo swiper-slide" style="background-image: url({{ URL::asset('/assets/img/post/'.$data->photo.'') }})">
                <div class="overlay"></div>

                    <div class="container full-height table">

                        <div class="table-cell">

                            <div class="slider-content align-center">
                                
                                <h5 class="slider-content-text c-gray" data-swiper-parallax="-300">{{ $data->Category->name }} - {{ $data->created_at->format('d M Y') }}</h5>
                                <h1 class="slider-content-title c-white" data-swiper-parallax="-200">{{ $data->title }}</h1>

                                <div class="main-slider-btn-wrap" data-swiper-parallax="-300">

                                    <a href="/blog/{{ $data->slug }}" class="btn btn-medium btn--secondary btn-hover-shadow">
                                        <span class="text">Read more</span>
                                        <span class="semicircle"></span>
                                    </a>

                                </div>

                            </div>
                        </div>

                    </div>
            </div>
            @endforeach
            {{-- @foreach($portofolio as $data)
            <div class="main-slider-bg-photo swiper-slide" style="background-image: url({!! URL::asset('/assets/img/portofolio/'.$data->photo.'') !!});">
                <div class="overlay"></div>

                    <div class="container full-height table">

                        <div class="table-cell">

                            <div class="slider-content align-center">
                                
                                <h5 class="slider-content-text c-gray" data-swiper-parallax="-300">{{ $data->PortofolioCategory->name }} - {{ $data->created_at->format('d M Y') }}</h5>
                                <h1 class="slider-content-title c-white" data-swiper-parallax="-200">{{ $data->title }}</h1> --}}

                                {{-- <div class="main-slider-btn-wrap" data-swiper-parallax="-300">

                                    <a href="/blog/{{ $data->slug }}" class="btn btn-medium btn--secondary btn-hover-shadow">
                                        <span class="text">Read more</span>
                                        <span class="semicircle"></span>
                                    </a>

                                </div> --}}

                            {{-- </div>
                        </div>

                    </div>
            </div>
            @endforeach --}}
        </div>

        <!--Prev next buttons-->

        <svg class="btn-next">
            <use xlink:href="#arrow-right"></use>
        </svg>

        <svg class="btn-prev">
            <use xlink:href="#arrow-left"></use>
        </svg>

        <!--Pagination tabs-->
        @php $no=1; @endphp
        <div class="slider-slides slider-shadow">
            @foreach($post as $data)
            <a href="#" class="slides-item bg-dark-color slide-active">
                <div class="content">
                    <div class="text-wrap">
                        <h4 class="slides-title">{{ $data->title }}</h4>
                    </div>
                    <div class="slides-number">{{ $no++ }}</div>
                </div>

                <div class="triangle-slides dark"></div>
            </a>
            @endforeach
        </div>
        {{-- <div class="slider-slides slider-shadow">
            @foreach($portofolio as $data)
            <a href="#" class="slides-item bg-dark-color slide-active">
                <div class="content">
                    <div class="text-wrap">
                        <h4 class="slides-title">{{ $data->title }}</h4>
                    </div>
                    <div class="slides-number">{{ $no++ }}</div>
                </div>

                <div class="triangle-slides dark"></div>
            </a>
            @endforeach
        </div> --}}
    </div>
</div>
@endsection