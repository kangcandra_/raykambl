@extends('layouts.front')
@section('css')
@endsection

@section('js')
@endsection

@section('content')
<div class="stunning-header stunning-header-bg-violet">
    <div class="stunning-header-content">
        <h1 class="stunning-header-title">Our Services</h1>
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a href="index-2.html">Home</a>
                <i class="seoicon-right-arrow"></i>
            </li>
            <li class="breadcrumbs-item active">
                <span href="#">Our Services</span>
                <i class="seoicon-right-arrow"></i>
            </li>
        </ul>
    </div>
</div>

{{-- item --}}
<div class="container">
    <div class="row pt120 pb30" style="padding: 50px;">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <div class="services-main">
                <div class="heading">
                    <h4 class="h1 heading-title">Full Services of Our Digital Agency</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>
                    <p class="heading-text">Qolor sit amet, consectetuer adipiscing elit,
                        sed diam nonummy nibham liber tempor cum soluta nobis.
                    </p>
                </div>

                <a href="21_seo_analysis.html" class="btn btn-medium btn--dark btn-hover-shadow">
                    <span class="text">Free SEO Analysis</span>
                    <span class="semicircle"></span>
                </a>
            </div>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <div class="servises-item bg-violet-color">
                <div class="servises-item__thumb">
                    <img src="/assets/front/img/services1.png" alt="service">
                </div>
                <div class="servises-item__content">
                    <h4 class="servises-title">Search Engine Optimization</h4>
                    <p class="servises-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                        sed diam nonummy nibh.
                    </p>
                </div>

                <a href="04_service_detail_seo.html" class="read-more"><i class="seoicon-right-arrow"></i></a>

            </div>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <div class="servises-item bg-primary-color">
                <div class="servises-item__thumb">
                    <img src="/assets/front/img/services2.png" alt="service">
                </div>
                <div class="servises-item__content">
                    <h4 class="servises-title">Local SEO</h4>
                    <p class="servises-text">Claritas est etiam processus dynamicus,
                        qui sequitur mutationem consuetudium lectorum quam nunc putamus.
                    </p>
                </div>

                <a href="05_service_details_localseo.html" class="read-more"><i class="seoicon-right-arrow"></i></a>

            </div>
        </div>
    </div>

    <div class="row pb120">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <div class="servises-item bg-secondary-color">
                <div class="servises-item__thumb">
                    <img src="/assets/front/img/services3.png" alt="service">
                </div>
                <div class="servises-item__content">
                    <h4 class="servises-title">Social Media
                        Marketing</h4>
                    <p class="servises-text">Investigationes demonstraverunt legere
                        me lius quod ii legunt saepius.
                    </p>
                </div>

                <a href="06_service_detail.html" class="read-more"><i class="seoicon-right-arrow"></i></a>

            </div>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <div class="servises-item bg-orange-color">
                <div class="servises-item__thumb">
                    <img src="/assets/front/img/services4.png" alt="service">
                </div>
                <div class="servises-item__content">
                    <h4 class="servises-title">Email Marketing</h4>
                    <p class="servises-text">Nam liber tempor cum soluta nobis eleifend
                        option congue nihil imperdiet doming id quod mazim placerat assum.
                    </p>
                </div>

                <a href="07_service_email_marketing.html" class="read-more"><i class="seoicon-right-arrow"></i></a>

            </div>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <div class="servises-item bg-green-color">
                <div class="servises-item__thumb">
                    <img src="/assets/front/img/services5.png" alt="service">
                </div>
                <div class="servises-item__content">
                    <h4 class="servises-title">Pay Per Click
                        Management</h4>
                    <p class="servises-text">Eodem modo typi, qui nunc nobis videntur parum clari,
                        fiant sollemnes in futurum.
                    </p>
                </div>

                <a href="08_service_ppc_management.html" class="read-more"><i class="seoicon-right-arrow"></i></a>

            </div>
        </div>
    </div>
</div>
{{-- end item --}}

{{-- feature item --}}
<div class="container-fluid">
    <div class="row bg-border-color medium-padding120" style="padding: 50px;">
        <div class="container">
            <div class="row pb30">
              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <div class="features-item">

                      <div class="features-item__thumb">
                          <img src="/assets/front/img/features1.png" alt="image">
                      </div>

                      <div class="features-content">
                        <a href="#" class="features-title">Local Search Strategy</a>
                        <p class="features-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                            sed diam nonummy nibh euismod tincidunt laoreet dolore magna.
                        </p>
                      </div>

                      <a class="read-more" href="#">Read More
                          <i class="seoicon-right-arrow"></i>
                      </a>

                  </div>
              </div>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="features-item">

                        <div class="features-item__thumb">
                            <img src="/assets/front/img/features2.png" alt="image">
                        </div>

                        <div class="features-content">
                            <a href="#" class="features-title">Maps Search</a>
                            <p class="features-text">Claritas est etiam processus dynamicus,
                                qui sequitur mutationem consuetudium lectorum investigationes demonstraverunt.
                            </p>
                        </div>

                        <a class="read-more" href="#">Read More
                            <i class="seoicon-right-arrow"></i>
                        </a>

                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="features-item">

                        <div class="features-item__thumb">
                            <img src="/assets/front/img/features3.png" alt="image">
                        </div>

                        <div class="features-content">
                            <a href="#" class="features-title">Link Building & Content</a>
                            <p class="features-text">Investigationes demonstraverunt lectores legere me lius
                                quod ii legunt saepius per seacula quarta decima et quinta decima.
                            </p>
                        </div>

                        <a class="read-more" href="#">Read More
                            <i class="seoicon-right-arrow"></i>
                        </a>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="features-item">

                        <div class="features-item__thumb">
                            <img src="/assets/front/img/features4.png" alt="image">
                        </div>

                        <div class="features-content">
                            <a href="#" class="features-title">Paid Search Advertising</a>
                            <p class="features-text">Claritas est etiam processus dynamicus, qui sequitur mutationem
                                consuetudium lectorum investigationes demonstraverunt.
                            </p>
                        </div>

                        <a class="read-more" href="#">Read More
                            <i class="seoicon-right-arrow"></i>
                        </a>

                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="features-item">

                        <div class="features-item__thumb">
                            <img src="/assets/front/img/features6.png" alt="image">
                        </div>

                        <div class="features-content">
                            <a href="#" class="features-title">Custom Website Design</a>
                            <p class="features-text">Investigationes demonstraverunt lectores legere me lius quod ii
                                legunt saepius per seacula quarta decima et quinta decima.
                            </p>
                        </div>

                        <a class="read-more" href="#">Read More
                            <i class="seoicon-right-arrow"></i>
                        </a>

                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="features-item">

                        <div class="features-item__thumb">
                            <img src="/assets/front/img/features5.png" alt="image">
                        </div>

                        <div class="features-content">
                            <a href="#" class="features-title">Custom Email Design</a>
                            <p class="features-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                                sed diam nonummy nibh euismod tincidunt laoreet dolore magna.
                            </p>
                        </div>

                        <a class="read-more" href="#">Read More
                            <i class="seoicon-right-arrow"></i>
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- end feature item --}}
@endsection