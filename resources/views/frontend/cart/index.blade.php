@extends('layouts.front')
@section('content')
<div class="container-fluid">
    <div class="row bg-border-color medium-padding120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="cart">
                        <h1 class="cart-title">In Your Shopping Cart: <span class="c-primary"> {{ $cart->count() }} items</span></h1>
                    </div>

                    <form action="#" method="post" class="cart-main">
                        <table class="shop_table cart">
                            <thead class="cart-product-wrap-title-main">
                            <tr>
                                <th class="product-remove">&nbsp;</th   >
                                <th class="product-thumbnail">Product</th>
                                <th class="product-price">Price</th>
                                <th class="product-quantity">Quantity</th>
                                <th class="product-subtotal">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cart as $data)
                            <tr class="cart_item">
                                <td class="product-remove">
                                    <a href="{{ route('front.cart.delete',$data->id) }}" class="product-del remove" title="Remove this item">
                                        
                                        <i class="seoicon-delete-bold"></i>
                                    </a>
                                </td>

                                <td class="product-thumbnail">

                                    <div class="cart-product__item">
                                        <a href="#">
                                            <img src="{{ asset('/assets/img/product/'.$data->photo.'') }}" alt="product" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" style="width:125px; height:150px;">
                                        </a>
                                        <div class="cart-product-content">
                                            <p class="cart-author">Model: {{ $data->model }}</p>
                                            <h5 class="cart-product-title">{{ $data->product_name }}</h5>
                                        </div>
                                    </div>
                                </td>

                                <td class="product-price">
                                    <h5 class="price amount">Rp.{{ number_format($data->price,2,".",".") }}</h5>
                                </td>

                                <td class="product-quantity">

                                    <div class="quantity">
                                        <a href="#" class="quantity-minus">-</a>
                                        <input title="Qty" class="email input-text qty text" type="text" value="{{ $data->quantity }}">
                                        <a href="#" class="quantity-plus">+</a>
                                    </div>

                                </td>

                                <td class="product-subtotal">
                                    <h5 class="total amount">Rp.{{ number_format($data->price*$data->quantity,2,".",".") }}</h5>
                                </td>

                            </tr>
                            @endforeach
                            </tbody>
                        </table>


                    </form>

                    <div class="cart-total">
                        <h3 class="cart-total-title">Cart Totals</h3>
                        {{--  <h5 class="cart-total-subtotal">Subtotal: <span class="price"></span></h5>  --}}
                        <h5 class="cart-total-total">Total: <span class="price">Rp. {{  number_format($price->total,2,".",".")}}</span></h5>
                        <a href="20_checkout.html" class="btn btn-medium btn--light-green btn-hover-shadow">
                            <span class="text">Proceed to Checkout</span>
                            <span class="semicircle"></span>
                        </a>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection