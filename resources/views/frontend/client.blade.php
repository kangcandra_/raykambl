@extends('layouts.front')
@section('js')
{!! Minify::javascript('/assets/front/js/isotope.pkgd.min.js')!!}
@endsection
@section('content')
<!-- Stunning-header -->

<div class="stunning-header stunning-header-bg-olive">
    <div class="stunning-header-content">
        <h1 class="stunning-header-title">Our Clients</h1>
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a href="{{ url('/') }}">Home</a>
                <i class="seoicon-right-arrow"></i>
            </li>
            <li class="breadcrumbs-item active">
                <span href="#">Our Clients</span>
                <i class="seoicon-right-arrow"></i>
            </li>
        </ul>
    </div>
</div>

<!-- End Stunning-header -->
<!-- Client items style-2 -->

<div class="container">
    <div class="row medium-padding120" style="padding: 20px;">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="heading align-center">
                <h4 class="h1 heading-title">We Help 150+ Customers</h4>
                <div class="heading-line">
                    <span class="short-line"></span>
                    <span class="long-line"></span>
                </div>
                <p class="heading-text">Pernah senang mendapatkan hasilnya? Bergabunglah dengan kami sekarang dan berkenan dengan pekerjaan kami!
                Mereka sudah memiliki website yang mereka banggakan.
                </p>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="clients-grid">

                <ul class="cat-list-bg-style align-center sorting-menu">
                    <li class="cat-list__item active" data-filter="*"><a href="#" class="">All Projects</a></li>
                    @foreach($category as $data)
                @if($data->Client->count() > 0)
                <li class="cat-list__item" data-filter=".client{{ $data->id }}"><a href="#" class="">{{ $data->name }}</a></li>
                @endif
                @endforeach
                </ul>


                <div class="row sorting-container" id="clients-grid" data-layout="masonry">
					@foreach($client as $data)
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 sorting-item client{{ $data->ClientCategory->id }}">
                        <div class="client-item-style2 col-3 bg-border-color mb30">

                            <div class="client-image">
                                <img src="{{ asset('/assets/img/client/'.$data->photo.'') }}" alt="logo" style="width: 250px; height: 180px;">
                                <img src="{{ asset('/assets/img/client/'.$data->photo.'') }}" alt="logo" style="width: 250px; height: 180px;" class="hover">
                            </div>

                            <h5 class="clients-item-title">{{ $data->name }}</h5>
                            <p class="clients-item-text">{{ $data->description }}
                            </p>

                            <a class="btn btn-medium btn-border c-primary" href="{{ $data->link }}">
                                <span class="text">Visit Site</span>
                                <span class="semicircle"></span>
                                <i class="seoicon-right-arrow"></i>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>

               {{--  <a href="#" class="load-more" id="load-more-button" data-load-link="clients-to-load.html" data-container="clients-grid">
                <span class="load-more-img-wrap">
                    <img src="/assets/front/img/load-more-line.png" alt="load-more">
                </span>
                    <span class="load-more-text">load more</span>
                </a> --}}

            </div>
        </div>
    </div>
</div>

<!-- End Client items style-2 -->
@endsection
