@extends('layouts.front')
@section('content')
<div class="stunning-header stunning-header-bg-gray">
    <div class="stunning-header-content">
        <h1 class="stunning-header-title">Product Details</h1>
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a href="{{ url('/') }}">Home</a>
                <i class="seoicon-right-arrow"></i>
            </li>
            <li class="breadcrumbs-item active">
                <span href="#">Product Details</span>
                <i class="seoicon-right-arrow"></i>
            </li>
        </ul>
    </div>
</div>
<!-- Product details -->

<div class="container">
    <div class="row medium-padding120">
        <div class="product-details">

            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <div class="product-details-thumb">

                    <div class="swiper-container" data-effect="fade">

                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <!-- Slides -->

                            <div class="product-details-img-wrap">
                                <img src="{{ asset('/assets/img/product/'.$product->photo.'') }}" style="width: 480px; height: 350px;" alt="product" data-swiper-parallax="-10">
                                <div class="sale" data-swiper-parallax="10">Sale</div>
                            </div>
                        </div>
                        <!-- If we need pagination -->
                        <div class="swiper-pagination"></div>
                    </div>

                </div>

            </div>
            <form action="{{ route('front.cart') }}" method="post"> 
                <input type="hidden" name="id" value="{{ $product->id }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" value="{{ $product->name }}" name="name">
                <input type="hidden" value="{{ $product->photo}}" name="photo">
                <input type="hidden" value="{{  $product->price}}" name="price">
                
                <div class="col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 col-sm-6 col-sm-offset-1 col-xs-12 col-xs-offset-0">
                    <div class="product-details-info">
                        <div class="product-details-info-price"><span class="del"> $23.99 </span>Rp {{ number_format($product->price,2,",",".") }}</div>
                        <h3 class="product-details-info-title">{{ $product->name }}</h3>
                        <div class="product-details-info-ratings">
                            <div class="ratings">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                            </div>
                            <a href="#" class="reviews">3 customer reviews</a>
                        </div>
                        <div class="heading-line">
                                <span class="short-line"></span>
                                <span class="long-line"></span>
                            </div>
                            <p class="heading-text"></p>
                                <div class="quantity">
                                    <a href="#" class="quantity-minus">-</a>
                                    <input title="Qty" name="quantity" class="email input-text qty text" type="text" value="1">
                                    <a href="#" class="quantity-plus">+</a>
                                </div>

                                <button type="submit" class="btn btn-medium btn--primary">
                                    <span class="text">Add to Cart</span>
                                    <i class="seoicon-commerce"></i>
                                    <span class="semicircle"></span>
                                </button>
                            </form>
                    </div>

                    <div class="product-details-add-info">
                        <div class="author">Category:
                            <a href="#" class="author-name">{{ $product->ProductCategory->name }}</a>
                        </div>
                        <div class="tags">Tags:
                            @foreach($tags as $tag)
                            <a class="tags-item" href="#">{{ $tag->name }},</a>
                            @endforeach
                        </div>
                        <div class="socials">Share:
                            <a href="#" class="social__item">
                                <i class="seoicon-social-facebook"></i>
                            </a>
                            <a href="#" class="social__item">
                                <i class="seoicon-social-twitter"></i>
                            </a>
                            <a href="#" class="social__item">
                                <i class="seoicon-social-linkedin"></i>
                            </a>
                            <a href="#" class="social__item">
                                <i class="seoicon-social-google-plus"></i>
                            </a>
                            <a href="#" class="social__item">
                                <i class="seoicon-social-pinterest"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="product-description">
            <div class="container">

                <ul class="product-description-control" role="tablist">

                    <li role="presentation" class="active">
                        <a href="#product-description" role="tab" data-toggle="tab" class="description control-item">Description</a>
                    </li>

                    <li role="presentation">
                        <a href="#product-reviews" role="tab" data-toggle="tab" class="reviews control-item">Reviews <span>(3)</span></a>
                    </li>

                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="product-description">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="product-description-title">Product Description:</div>
                                <p class="product-description-text"{!! $product->description !!}</p>
                            </div>

                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="product-reviews">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="product-description-title">3 Reviews for “<span class="c-primary">{{ $product->name }}</span>”</div>

                                <ol class="comments__list-review">

                                    <li class="comments__item-review">

                                        <div class="comment-entry comment comments__article">

                                            <div class="comments__avatar-review">

                                                <img src="/assets/front/img/reviews-avatar1.png" alt="avatar">
                                            </div>
                                            <div class="comments__body ovh">

                                                <div class="ratings">
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                                </div>

                                                <header class="comment-meta comments__header-review">

                                                    <cite class="fn url comments__author-review">
                                                        <a href="#" rel="external" class=" ">Jonathan Simpson</a>
                                                    </cite>

                                                    <a href="#" class="comments__time-review">
                                                        <time class="published" datetime="2016-04-20 12:00:00">April 20, 2016
                                                        </time>
                                                    </a>

                                                </header>

                                                <div class="comment-content comment">
                                                    <p>Mirum est notare quam littera gothica, quam nunc putamus parum claram,
                                                        anteposuerit litterarum formas humanitatis per seacula quarta decima
                                                        et quinta decima.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="comments__item-review">
                                        <div class="comment-entry comment comments__article">
                                            <div class="comments__avatar-review">
                                                <img src="/assets/front/img/reviews-avatar2.png" alt="avatar">
                                            </div>
                                            <div class="comments__body ovh">

                                                <div class="ratings">
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </div>

                                                <header class="comment-meta comments__header-review">

                                                    <cite class="fn url comments__author-review">
                                                        <a href="#" rel="external" class=" ">Angelina Johnson</a>
                                                    </cite>

                                                    <a href="#" class="comments__time-review">
                                                        <time class="published" datetime="2016-04-20 12:00:00">April 20, 2016
                                                        </time>
                                                    </a>
                                                </header>

                                                <div class="comment-content comment">
                                                    <p>Qothica, quam nunc putamus parum claram, anteposuerit litterarum formas.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="comments__item-review">

                                        <div class="comment-entry comment comments__article">

                                            <div class="comments__avatar-review">

                                                <img src="/assets/front/img/reviews-avatar3.png" alt="avatar">

                                            </div>

                                            <div class="comments__body ovh">

                                                <div class="ratings">
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                                </div>

                                                <header class="comment-meta comments__header-review">

                                                    <cite class="fn url comments__author-review">
                                                        <a href="#" rel="external" class=" ">Philip Demarco</a>
                                                    </cite>

                                                    <a href="#" class="comments__time-review">
                                                        <time class="published" datetime="2016-04-20 12:00:00">April 20, 2016
                                                        </time>
                                                    </a>

                                                </header>

                                                <div class="comment-content comment">
                                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                                                        nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat
                                                        volutpat. Eodem modo typi, qui nunc nobis videntur parum clari, fiant
                                                        sollemnes in futurum autem vel eum iriure dolor in hendrerit in
                                                        vulputate velit esse molestie consequat.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ol>
                            </div>

                        </div>

                        <div class="row">
                            <div class="add-review">
                                <div class="col-lg-12">
                                    <div class="product-description-title">Add a Review</div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="your-rating">Your Rating:
                                        <div class="ratings">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="contact-form">
                                        <form>

                                            <div class="row">

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <input class="email input-standard-grey" placeholder="Your Name" type="text">

                                                    <input class="email input-standard-grey" placeholder="Email Address" type="text">
                                                </div>


                                            </div>

                                            <div class="row">

                                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

                                                    <textarea class="email input-standard-grey" placeholder="Your Review"></textarea>

                                                </div>

                                            </div>

                                            <button class="btn btn-medium btn--breez btn-hover-shadow">
                                                <span class="text">Submit</span>
                                                <span class="semicircle"></span>
                                            </button>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection