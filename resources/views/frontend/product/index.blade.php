@extends('layouts.front')
@section('js')
{!! Minify::javascript('/assets/front/js/isotope.pkgd.min.js')!!}
@endsection
@section('content')
<!-- Stunning header -->
<div class="stunning-header stunning-header-bg-breez">
    <div class="stunning-header-content">
        <h1 class="stunning-header-title">Product</h1>
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a href="{{ url('/') }}">Home</a>
                <i class="seoicon-right-arrow"></i>
            </li>
            <li class="breadcrumbs-item active">
                <span href="#">Product</span>
                <i class="seoicon-right-arrow"></i>
            </li>
        </ul>
    </div>
</div>
<!-- End Stunning header -->
<!-- Case Item -->

<div class="container">
    <div class="row medium-padding" style="padding: 50px;">

        <div class="col-lg-12">

            <div class="heading align-center">
                <h4 class="h1 heading-title">We Help Over 80 Companies</h4>
            </div>

            <ul class="cat-list align-center sorting-menu">
                <li class="cat-list__item active" data-filter="*"><a href="#" class="">All Product</a></li>
                @foreach($category as $data)
                @if($data->Product->count() > 0)
                <li class="cat-list__item" data-filter=".product{{ $data->id }}"><a href="#" class="">{{ $data->name }}</a></li>
                @endif
                @endforeach
            </ul>


            <div class="row sorting-container" data-layout="fitRows">
                <div class="grid-sizer col-lg-4 col-md-4"></div>
                @foreach($product as $data)
                @if($loop->first)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 sorting-item product{{ $data->productCategory->id }}">
                    <div class="books-item new">
                        <div class="books-item-thumb">
                            <img src="{{ asset('/assets/img/product/'.$data->photo.'') }}" style="width: 170px; height: 222px;" alt="book">
                            <div class="new">New</div>
                            <div class="sale">Sale</div>
                            <div class="overlay overlay-books"></div>
                        </div>

                        <div class="books-item-info">
                            <div class="books-author"></div>
                            <h5 class="books-title">{{ $data->name }}</h5>

                            <div class="books-price">Rp {{ number_format($data->price,2,",",".") }}</div>
                        </div>

                        <a href="/product/{{ $data->slug }}" class="btn btn-small btn--dark add">
                            <span class="text">Lihat Produk</span>
                            <i class="seoicon-commerce"></i>
                        </a>

                    </div>
                </div>
                @else
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 sorting-item product{{ $data->productCategory->id }}">
                    <div class="books-item new">
                        <div class="books-item-thumb">
                            <img src="{{ asset('/assets/img/product/'.$data->photo.'') }}" style="width: 170px; height: 222px;" alt="book">
                            <div class="sale">Sale</div>
                            <div class="overlay overlay-books"></div>
                        </div>

                        <div class="books-item-info">
                            {{--  <div class="books-author">Douglas Brown</div>  --}}
                            <h5 class="books-title">{{ $data->name }}</h5>

                            <div class="books-price">Rp {{ number_format($data->price,2,",",".") }}</div>
                        </div>

                        <a href="/product/{{ $data->slug }}" class="btn btn-small btn--dark add">
                            <span class="text">Lihat Produk</span>
                            <i class="seoicon-commerce"></i>
                        </a>

                    </div>
                </div>
                @endif
                @endforeach

            </div>

            <div class="row">
            {{ $product->links('vendor.pagination.custom') }}

            </div>
        </div>

    </div>
</div>

<!-- End Case Item -->
@endsection