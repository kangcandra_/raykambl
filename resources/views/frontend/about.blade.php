@extends('layouts.front')
@section('css')
 <link rel="stylesheet" type="text/css" href="/assets/front/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/front/css/swiper.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/front/css/primary-menu.css">
    <link rel="stylesheet" type="text/css" href="/assets/front/css/magnific-popup.css">
    <style>
    .img-circle {
        border-radius: 50%;
    }
    </style>
@endsection
@section('js')
 <script src="/assets/front/js/time-line.js"></script>
@endsection
@section('content')
<!-- Stunning header -->

<div class="stunning-header stunning-header-bg-lightblue">
    <div class="stunning-header-content">
        <h1 class="stunning-header-title">Tentang Assalaam Studio</h1>
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a href="{{ url('/') }}">Home</a>
                <i class="seoicon-right-arrow"></i>
            </li>
            <li class="breadcrumbs-item active">
                <span href="#">About</span>
                <i class="seoicon-right-arrow"></i>
            </li>
        </ul>
    </div>
</div>

<!-- End Stunning header -->
<!--  Overlay Search -->

<div class="overlay_search">
    <div class="container">
        <div class="row">
            <div class="form_search-wrap">
                <form>
                    <input class="overlay_search-input" placeholder="Type and hit Enter..." type="text">
                    <a href="#" class="overlay_search-close">
                        <span></span>
                        <span></span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- End Overlay Search -->
<div class="container">
    <div class="row pt120" style="padding: 40px;">
        <div class="col-lg-12">
            <div class="heading mb30">
                <h4 class="h1 heading-title">Cerita Singkat Tentang Assalaam Studio</h4>
                <div class="heading-line">
                    <span class="short-line"></span>
                    <span class="long-line"></span>
                </div>

                <h5 class="heading-subtitle">
                </h5>
            </div>
        </div>

        <div class="col-lg-12">
            <p>Kami adalah salah satu Unit Produksi di suatu instansi pendidikan yaitu <a href="https://smkassalaambandung.sch.id">SMK Assalaam Bandung</a>, Kami adalah Penyedia Layanan Jasa Pembuatan Aplikasi Web, Mobile Android, Multimedia dan Perawatan Komputer Profesional di Bandung yang selalu mengutamakan pelayanan jasa terbaik untuk meningkatkan performa bisnis dan penjualan anda. Apapun masalah anda, baik kecil sampai dengan yang kompleks, kami selalu senang dan siap membantu setiap permasalahan IT anda dengan harga yang kompetitif. Tim teknisi kami yang berpengalaman dapat memecahkan setiap permasalahan anda dengan memberikan solusi yang cepat dan tepat. Kami juga menyediakan layanan konsultasi atau perbaikan ke tempat anda ataupun melalui telepon atau remote.
            <br><br>
            Tim kami akan selalu siap membantu dan memahami permasalahan anda yang anda hadapi serta merespon permasalahan anda dengan cepat. Hal ini sangat penting untuk segera ditangani oleh tim yang ahli dan terpercaya serta berpengalaman seperti kami Assalaam Studio. Jangan tunda-tunda masalah anda yang akan mengakibatkan bisnis anda menjadi terhambat hanya karena gara-gara masalah IT.
            </p>
        </div>

    </div>
</div>
<!-- Time line -->

<div class="container">

<div class="row">

<div class="col-lg-12">

    <section class="cd-horizontal-timeline">
        <div class="timeline">
            <div class="events-wrapper">
                <div class="events">
                    <ol>
                        <li><a href="#0" data-date="01/01/2016" class="selected">2016</a></li>
                        {{-- <li><a href="#0" data-date="08/11/2017">2017</a></li> --}}
                        {{-- <li><a href="#0" data-date="05/01/2013">2017</a></li> --}}
                        {{-- <li><a href="#0" data-date="10/09/2014">2014</a></li>
                        <li><a href="#0" data-date="07/05/2015">2015</a></li>
                        <li><a href="#0" data-date="20/12/2016">2016</a></li> --}}
                        <!-- other dates here -->
                    </ol>

                    <span class="filling-line" aria-hidden="true"></span>
                </div> <!-- .events -->
            </div> <!-- .events-wrapper -->

           {{--  <ul class="cd-timeline-navigation">
                <li><a href="#0" class="prev inactive seoicon-play">Prev</a></li>
                <li><a href="#0" class="next seoicon-play">Next</a></li>
            </ul> --}} <!-- .cd-timeline-navigation -->
        </div> <!-- .timeline -->

        <div class="events-content">
            <ol>
                <li class="selected" data-date="04/01/2016">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 table-cell">
                            <div class="time-line-thumb">
                                <img src="/assets/front/img/time-line-thumb.png" alt="time-line">
                            </div>
                        </div>

                        <div class="col-lg-8 col-lg-offset-1 col-md-8 col-md-offset-1 col-sm-9 col-xs-12 table-cell">
                            <div class="time-line-content">
                                <h6 class="time-line-subtitle">Januari, 2016</h6>
                                <h5 class="time-line-title">Pendirian Assalaam Studio</h5>
                                <p class="time-line-text">Assalaam Studio Assalaam berdiri dan diresmikan pada tanggal 4 Januari tahun 2016.Pada tahun tersebut Assalaam Studio baru memulai pelayanan dan jasa dibidang pembuatan Aplikasi Website, Mobile Android dan juga Maintenance Komputer serta Multimedia.
                                </p>
                            </div>
                        </div>
                    </div>

                </li>

                {{-- <li data-date="08/11/2017">

                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 table-cell">
                            <div class="time-line-thumb">
                                <img src="/assets/front/img/time-line-thumb.png" alt="time-line">
                            </div>
                        </div>

                        <div class="col-lg-8 col-lg-offset-1 col-md-8 col-md-offset-1 col-sm-9 col-xs-12 table-cell">
                            <div class="time-line-content">
                                <h6 class="time-line-subtitle">Juni, 2017</h6>
                                <h5 class="time-line-title"> Multimedia</h5>
                                <p class="time-line-text">Assalaam Studio memulai 
                                </p>
                            </div>
                        </div>
                    </div>

                </li> --}}

                {{-- <li data-date="05/01/2013">

                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 table-cell">
                            <div class="time-line-thumb">
                                <img src="/assets/front/img/time-line-thumb.png" alt="time-line">
                            </div>
                        </div>

                        <div class="col-lg-8 col-lg-offset-1 col-md-8 col-md-offset-1 col-sm-9 col-xs-12 table-cell">
                            <div class="time-line-content">
                                <h6 class="time-line-subtitle">Jan, 2013</h6>
                                <h5 class="time-line-title">Foundation of the Company</h5>
                                <p class="time-line-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt
                                    saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium
                                    lectorum. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit
                                    lobortis nisl ut aliquip ex commodo est usus legentis in iis qui facit eorum.
                                </p>
                            </div>
                        </div>
                    </div>

                </li>

                <li class="" data-date="10/09/2014">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 table-cell">
                            <div class="time-line-thumb">
                                <img src="/assets/front/img/time-line-thumb.png" alt="time-line">
                            </div>
                        </div>

                        <div class="col-lg-8 col-lg-offset-1 col-md-8 col-md-offset-1 col-sm-9 col-xs-12 table-cell">
                            <div class="time-line-content">
                                <h6 class="time-line-subtitle">Sept, 2014</h6>
                                <h5 class="time-line-title">Foundation of the Company</h5>
                                <p class="time-line-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt
                                    saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium
                                    lectorum. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit
                                    lobortis nisl ut aliquip ex commodo est usus legentis in iis qui facit eorum.
                                </p>
                            </div>
                        </div>
                    </div>
                </li>

                <li data-date="07/05/2015">

                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 table-cell">
                            <div class="time-line-thumb">
                                <img src="/assets/front/img/time-line-thumb.png" alt="time-line">
                            </div>
                        </div>

                        <div class="col-lg-8 col-md-8 col-md-offset-1 col-sm-12 col-xs-12 col-lg-offset-1 table-cell">
                            <div class="time-line-content">
                                <h6 class="time-line-subtitle">May, 2015</h6>
                                <h5 class="time-line-title">Foundation of the Company</h5>
                                <p class="time-line-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt
                                    saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium
                                    lectorum. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit
                                    lobortis nisl ut aliquip ex commodo est usus legentis in iis qui facit eorum.
                                </p>
                            </div>
                        </div>
                    </div>

                </li>

                <li data-date="20/12/2016">

                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 table-cell">
                            <div class="time-line-thumb">
                                <img src="/assets/front/img/time-line-thumb.png" alt="time-line">
                            </div>
                        </div>

                        <div class="col-lg-8 col-md-8 col-md-offset-1 col-sm-12 col-xs-12 col-lg-offset-1 table-cell">
                            <div class="time-line-content">
                                <h6 class="time-line-subtitle">Dec, 2016</h6>
                                <h5 class="time-line-title">Foundation of the Company</h5>
                                <p class="time-line-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt
                                    saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium
                                    lectorum. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit
                                    lobortis nisl ut aliquip ex commodo est usus legentis in iis qui facit eorum.
                                </p>
                            </div>
                        </div>
                    </div>

                </li> --}}

                <!-- other descriptions here -->
            </ol>
        </div> <!-- .events-content -->
    </section>

</div>

</div>

</div>

<!-- End Time line -->
<!-- Slider Profit -->
<div class="section bg-greendark-color">
    <div class="container">

        <div class="slider-profit-wrap">

            <!-- Slider main container -->
            <div class="swiper-container auto-height pagination-vertical" data-direction="vertical" data-loop="false" data-mouse-scroll="true">

                <div class="swiper-wrapper">
                    <div class="slider-profit swiper-slide">
                        <div class="row medium-padding120">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="heading">
                                    <h4 class="h1 heading-title c-white mb30">We Work for Your Profit</h4>
                                    <p class="c-white">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
                                        nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat wisi enim ad veniam.
                                    </p>
                                </div>
                                <a href="21_seo_analysis.html" class="btn btn-medium btn--dark btn-hover-shadow">
                                    <span class="text">Read More</span>
                                    <span class="semicircle"></span>
                                </a>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="slider-profit-thumb">
                                    <img src="/assets/front/img/profit.png" alt="profit">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="slider-profit swiper-slide">
                        <div class="row medium-padding120">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="heading">
                                    <h4 class="h1 heading-title c-white mb30">We Work for Your Profit</h4>
                                    <p class="c-white">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
                                        nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat wisi enim ad veniam.
                                    </p>
                                </div>

                                <a href="21_seo_analysis.html" class="btn btn-medium btn--dark btn-hover-shadow">
                                    <span class="text">Read More</span>
                                    <span class="semicircle"></span>
                                </a>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="slider-profit-thumb">
                                    <img src="/assets/front/img/profit.png" alt="profit">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="slider-profit swiper-slide">
                        <div class="row medium-padding120">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="heading">
                                    <h4 class="h1 heading-title c-white mb30">We Work for Your Profit</h4>
                                    <p class="c-white">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
                                        nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat wisi enim ad veniam.
                                    </p>
                                </div>

                                <a href="21_seo_analysis.html" class="btn btn-medium btn--dark btn-hover-shadow">
                                    <span class="text">Read More</span>
                                    <span class="semicircle"></span>
                                </a>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="slider-profit-thumb">
                                    <img src="/assets/front/img/profit.png" alt="profit">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="slider-profit swiper-slide">
                        <div class="row medium-padding120">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="heading">
                                    <h4 class="h1 heading-title c-white mb30">We Work for Your Profit</h4>
                                    <p class="c-white">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
                                        nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat wisi enim ad veniam.
                                    </p>
                                </div>

                                <a href="21_seo_analysis.html" class="btn btn-medium btn--dark btn-hover-shadow">
                                    <span class="text">Read More</span>
                                    <span class="semicircle"></span>
                                </a>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="slider-profit-thumb">
                                    <img src="/assets/front/img/profit.png" alt="profit">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- If we need pagination -->
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>
</div>
<!-- End Slider Profit -->
<!-- Team members -->

<div class="container">
    <div class="row pt120 mb30" style="padding: 50px;">

        <div class="col-lg-12">
            <div class="heading align-center">
                <h4 class="h1 heading-title">Meet Our Team</h4>
                <div class="heading-line">
                    <span class="short-line"></span>
                    <span class="long-line"></span>
                </div>
                <p class="heading-text">Tim Yang Handal dan berkualitas tinggi siap membantu anda</p>
            </div>
        </div>

    </div>

    <div class="row pb120">
        @foreach($team as $data)
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="teammembers-item">
                <img style="height: 262px; width: 262px;" class="img-circle" src="{{ asset('/assets/img/team/'.$data->photo.'') }}" alt="team">
                <h5 class="teammembers-item-name">{{ $data->name }}</h5>
                <p class="teammembers-item-prof">{{ $data->Division->name }}</p>

                <div class="socials">
                    @if($data->fb != null)
                    <a href="https://facebook.com/{{ $data->fb }}" class="social__item">
                        <img src="/assets/front/svg/circle-facebook.svg" alt="facebook">
                    </a>
                    @endif
                    @if($data->tw != null)
                    <a href="https://twitter.com/{{ $data->tw }}" class="social__item">
                        <img src="/assets/front/svg/twitter.svg" alt="twitter">
                    </a>
                    @endif
                    @if($data->gp != null)
                    <a href="https://plus.google.com/{{ $data->gp }}" class="social__item">
                        <img src="/assets/front/svg/google.svg" alt="google">
                    </a>
                    @endif
                    @if($data->yt != null)
                    <a href="https://youtube.com/channel/{{ $data->yt }}" class="social__item">
                        <img src="/assets/front/svg/youtube.svg" alt="youtube">
                    </a>
                    @endif
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>

<!-- End Team members -->
<!-- Info boxes -->


<div class="container-fluid">
    <div class="row medium-padding120 bg-border-color" style="padding: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                    <div class="info-box--standard">
                        <div class="info-box-image f-none">
                            <img src="/assets/front/img/info-box20.png" alt="image">
                        </div>
                        <div class="info-box-content">
                            <h5 class="info-box-title">Excellent Support</h5>
                            <p class="text">Kami terus-menerus mengukur apa yang kami lakukan, tidak hanya dalam hal seberapa bagus tampilannya, tapi dari segi hasil yang diberikannya. Kami berkomitmen untuk memberikan hasil bagus yang akan meningkatkan bisnis Anda dan kami tidak akan puas dengan hal-hal yang kurang.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                    <div class="info-box--standard">
                        <div class="info-box-image f-none">
                            <img src="/assets/front/img/info-box21.png" alt="image">
                        </div>
                        <div class="info-box-content">
                            <h5 class="info-box-title">Awesome Team</h5>
                            <p class="text">Anda akan senang bekerja sama dengan kami. Kami berharap setelah pertemuan pertama, Anda akan melihat apa yang membedakan kami dan menyadari aset berharga apa yang bisa kami dapatkan untuk perusahaan Anda.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                    <div class="info-box--standard">
                        <div class="info-box-image f-none">
                            <img src="/assets/front/img/info-box22.png" alt="image">
                        </div>
                        <div class="info-box-content">
                            <h5 class="info-box-title">Faster Performance</h5>
                            <p class="text">Anda Akan senang bekerja sama dengan kami, karena kami memilki tim yang sangat cepat dalam mengerjakan sebuah project
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End Info boxes -->
<!-- Our vision -->

<div class="row medium-padding120 bg-boxed-black">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="heading">

                        <h4 class="h1 heading-title c-white">Quality Skills</h4>

                        <div class="heading-line">
                            <span class="short-line"></span>
                            <span class="long-line"></span>
                        </div>

                        <p class="mb30">Skill yang sangat tinggi akan sangat mempermudah kami untuk melakukan pekerjaan menjadi lebih cepat.
                        </p>

                    </div>

                    {{-- <a href="21_seo_analysis.html" class="btn btn-medium btn--olive btn-hover-shadow mb30">
                        <span class="text">Free SEO Consultation</span>
                        <span class="semicircle"></span>
                    </a> --}}
                </div>

                <div class="col-lg-7 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12">
                    <div class="skills">
                        <div class="skills-item">
                            <div class="skills-item-info">
                                <span class="skills-item-title">Web Design & Development</span>
                                <span class="skills-item-count"><span class="count-animate" data-speed="1000" data-refresh-interval="50" data-to="95" data-from="0">95</span><span class="units">%</span></span>
                            </div>
                            <div class="skills-item-meter">
                                <span class="skills-item-meter-active bg-orange-color border-orange-color skills-animate" style="width: 95%; opacity: 1;">
                                </span>
                            </div>
                        </div>

                        <div class="skills-item">
                            <div class="skills-item-info">
                                <span class="skills-item-title">Maintenance Computer</span>
                                <span class="skills-item-count"><span class="count-animate" data-speed="1000" data-refresh-interval="50" data-to="100" data-from="0">100</span><span class="units">%</span></span>
                            </div>

                            <div class="skills-item-meter">
                                <span class="skills-item-meter-active bg-green-color border-green-color skills-animate" style="width: 100%; opacity: 1;">
                                </span>
                            </div>
                        </div>

                        <div class="skills-item">
                            <div class="skills-item-info">
                                <span class="skills-item-title">Mobile Android Development</span>
                                <span class="skills-item-count"><span class="count-animate" data-speed="1000" data-refresh-interval="50" data-to="90" data-from="0">90</span><span class="units">%</span></span>
                            </div>

                            <div class="skills-item-meter">
                                <span class="skills-item-meter-active bg-secondary-color border-secondary-color skills-animate" style="width: 90%; opacity: 1;">
                                </span>
                            </div>
                        </div>

                        <div class="skills-item">
                            <div class="skills-item-info">
                                <span class="skills-item-title">Graphic Design</span>
                                <span class="skills-item-count"><span class="count-animate" data-speed="1000" data-refresh-interval="50" data-to="97" data-from="0">97</span><span class="units">%</span></span>
                            </div>

                            <div class="skills-item-meter">
                                <span class="skills-item-meter-active bg-primary-color border-primary-color skills-animate" style="width: 97%; opacity: 1;">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>


<!-- End Our vision -->
<!-- Testimonial slider -->


{{-- <div class="container-fluid">
    <div class="row medium-padding120 bg-border-color">
        <div class="container">

            <div class="testimonial-arrow-slider">

                <div class="row">

                    <div class="col-lg-12">
                        <div class="heading">
                            <h4 class="h1 heading-title">Our Customers Say</h4>
                            <a href="#" class="read-more">Read All Testimonial
                                <i class="seoicon-right-arrow"></i>
                            </a>
                            <div class="heading-line">
                                <span class="short-line"></span>
                                <span class="long-line"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="swiper-container pagination-bottom">

                        <div class="swiper-wrapper">
                            <div class="case-slider-item swiper-slide">
                                @foreach($testimonial as $data)
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="testimonial-item testimonial-arrow mb30">
                                        <div class="testimonial-text">
                                            {{ $data->description }}
                                        </div>

                                        <div class="author-info-wrap table">
                                            <div class="testimonial-img-author table-cell">
                                                <img src="{{ asset('/assets/img/testimonial/'.$data->photo.'') }}" style="width: 53px; height: 64px;" alt="author" class="circle">
                                            </div>
                                            <div class="author-info table-cell">
                                                <h6 class="author-name">{{ $data->name }}</h6>
                                                <div class="author-company c-primary">{{ $data->company }}</div>
                                            </div>
                                        </div>

                                        <div class="quote">
                                            <i class="seoicon-quotes"></i>
                                        </div>

                                    </div>
                                </div>
                                @endforeach
                            </div>

                        </div>

                        <!-- If we need pagination -->
                        <div class="swiper-pagination"></div>

                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<!-- End Testimonial slider -->
<!-- Clients -->

<div class="container">
    <div class="row medium-padding120">
        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="heading">
                <h4 class="h1 heading-title">Our Valuable Clients</h4>
                <div class="heading-line">
                    <span class="short-line"></span>
                    <span class="long-line"></span>
                </div>
                <p class="heading-text">Mirum est notare quam littera gothica, quam nunc putamus parum claram,
                    anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima
                </p>
            </div>

            <a href="{{ url('/client') }}" class="btn btn-medium btn--dark mb30">
                <span class="text">View all Customers</span>
                <span class="semicircle"></span>
            </a>
        </div>

        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="clients-item-wrap">
                    @foreach($client as $data)
                    <div class="client-item mb60">
                        <a href="{{ $data->link }}" class="client-image">
                            <img src="{{ asset('/assets/img/client/'.$data->photo.'') }}" style="height: 116px; width: 210px;" alt="logo" class="hover">
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End Clients --> --}}
@endsection