@extends('layouts.front')
@section('css')
    <style>
        .img-circle {
            border-radius: 50%;
        }

    </style>
@endsection
@section('content')
    <!-- Stunning header -->

    <div class="stunning-header stunning-header-bg-brown">
        <div class="stunning-header-content">
            <h1 class="stunning-header-title">Testimonials</h1>
            <ul class="breadcrumbs">
                <li class="breadcrumbs-item">
                    <a href="{{ url('/') }}">Home</a>
                    <i class="seoicon-right-arrow"></i>
                </li>
                <li class="breadcrumbs-item active">
                    <span href="#">Testimonials</span>
                    <i class="seoicon-right-arrow"></i>
                </li>
            </ul>
        </div>
    </div>

    <!-- End Stunning header -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-xs-12">
                <div class="heading medium-padding" style="padding: 50px;">
                    <h4 class="h1 heading-title">Apa Pendapat Pelanggan Tentang Kami</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>
                    <p class="heading-text">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming
                        id quod mazim placerat facer possim assum. Mirum est notare quam littera gothica, quam nunc putamus
                        parum claram, anteposuerit litterarum formas humanitatis.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- Testimonial -->

    <div class="container-fluid">
        <div class="row bg-border-color medium-padding120" style="padding: 50px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        @foreach ($testimonial as $data)
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                                <div class="testimonial-item testimonial-arrow mb30">
                                    <div class="testimonial-text">
                                        <p>{{ $data->description }}</p>
                                    </div>

                                    <div class="author-info-wrap table">
                                        <div class="testimonial-img-author table-cell">
                                            <img src="{{ asset('/assets/img/testimonial/' . $data->photo . '') }}"
                                                class="img-circle" style="width: 100px; height: 100px;" alt="author">
                                        </div>
                                        <div class="author-info table-cell">
                                            <h6 class="author-name">{{ $data->name }}</h6>
                                            <div class="author-company c-primary">{{ $data->company }}</div>
                                        </div>
                                    </div>

                                    <div class="quote">
                                        <i class="seoicon-quotes"></i>
                                    </div>

                                </div>
                            </div>
                        @endforeach
                    </div>
                    {{ $testimonial->links('vendor.pagination.custom') }}
                </div>

            </div>
        </div>
    </div>

    <!-- End Testimonial -->
@endsection
