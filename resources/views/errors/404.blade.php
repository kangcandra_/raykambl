<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Error Page</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
     <link rel="icon" type="image/png" href="{{ asset('assets/front/images/logo1.png') }}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{ asset('assets/front/images/logo1.png') }}" sizes="16x16" />
    <!-- Le styles -->
    {!! Minify::stylesheet(array('/error/css/style.css'))!!}

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
     <link rel="shortcut icon" href="{!! asset('front/assets/images/logo1.png')!!}" >
  </head>

  <body>
        
        <div class="field">
            <div class="wrapper">
                <div class="ufo-text">
                    <img src="/error/images/oh-no.png" class="oh-no" alt="">
                    <h1>This page has been Abducted!</h1>
                    <span class="lines">Please GO BACK</span>
                    
                    <div class="menu">
                        <nav>
                            <ul>
                                <li><a href="{{ url('/') }}">Return Home</a></li>
                                <li><a href="{{ url('/contact') }}">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                    
                    {{-- <div class="search">
                        <form>
                            <input type="text" placeholder="Start your search here">
                            <button type="submit"></button>
                        </form>
                    </div> --}}
                </div>
                <img src="{!! asset('/error/images/light.png')!!}" class="light" alt="">
                <img src="{!! asset('/error/images/ufo.png')!!}" class="ufo" alt="">
                <img src="{!! asset('/error/images/smoke.png')!!}" class="smoke" alt="">
                
                <div class="footer">
                    <nav>
                        <ul>
                            <li><a href="https://www.facebook.com/smkassalaam/" class="icn fb"></a></li>
                            <li><a href="https://twitter.com/smkassalaam/" class="icn tw"></a></li>
                        </ul>
                    </nav>
                    <span class="copy">Assalaam Studio &copy; SMK Assalaam Bandung 2017  </span>
                </div>
            </div>
        </div>
        
        {!! Minify::javascript(array('/error/js/jquery-1.11.3.min.js'))!!}
        {!! Minify::javascript(array('/error/js/jquery.placeholder.js'))!!}
        {!! Minify::javascript(array('/error/js/custom.js'))!!}
  </body>
</html>


