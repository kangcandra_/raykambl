@extends('layouts.backend')
@section('css')
@endsection

@section('js')
@endsection

@section('title')
<i class="icon-diamond"></i> <span class="title">Rayka Mebel</span>
@endsection
@section('subtitle')
Dashboard
@endsection

@section('content')
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <a href="{{ route('backend.portofolio.index') }}">
    <div class="dashboard-stat2 bordered">
        <div class="display">
            <div class="number">
                <h3 class="font-green-sharp">
                    <span data-counter="counterup" data-value="{{ count($portofolio) }}"></span>
                    <small class="font-green-sharp"></small>
                </h3>
                <small>TOTAL Portofolio</small>
            </div>
            <div class="icon">
                <i class="icon-pie-chart"></i>
            </div>
        </div>
        <div class="progress-info">
            <div class="progress">
                <span style="width: 100%;" class="progress-bar progress-bar-success green-sharp">
                    <span class="sr-only">100% progress</span>
                </span>
            </div>
            <div class="status">
                <div class="status-title"> progress </div>
                <div class="status-number"> 100% </div>
            </div>
        </div>
    </div>
    </a>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <a href="{{ route('backend.client.index') }}">
    <div class="dashboard-stat2 bordered">
        <div class="display">
            <div class="number">
                <h3 class="font-red-haze">
                    <span data-counter="counterup" data-value="{{ count($client) }}"></span>
                </h3>
                <small>Client</small>
            </div>
            <div class="icon">
                <i class="icon-like"></i>
            </div>
        </div>
        <div class="progress-info">
            <div class="progress">
                <span style="width: 100%;" class="progress-bar progress-bar-success red-haze">
                    <span class="sr-only">100% progress</span>
                </span>
            </div>
            <div class="status">
                <div class="status-title"> progress </div>
                <div class="status-number"> 100% </div>
            </div>
        </div>
    </div>
    </a>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <a href="{{ route('backend.product.index') }}">
    <div class="dashboard-stat2 bordered">
        <div class="display">
            <div class="number">
                <h3 class="font-blue-sharp">
                    <span data-counter="counterup" data-value="{{ count($product) }}"></span>
                </h3>
                <small>Product</small>
            </div>
            <div class="icon">
                <i class="icon-basket"></i>
            </div>
        </div>
        <div class="progress-info">
            <div class="progress">
                <span style="width: 100%;" class="progress-bar progress-bar-success blue-sharp">
                    <span class="sr-only">100% progress</span>
                </span>
            </div>
            <div class="status">
                <div class="status-title"> progress </div>
                <div class="status-number"> 100% </div>
            </div>
        </div>
    </div>
    </a>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <a href="{{ route('backend.users.index') }}">
    <div class="dashboard-stat2 bordered">
        <div class="display">
            <div class="number">
                <h3 class="font-purple-soft">
                    <span data-counter="counterup" data-value="{{ count($user) }}"></span>
                </h3>
                <small>NEW USERS</small>
            </div>
            <div class="icon">
                <i class="icon-user"></i>
            </div>
        </div>
        <div class="progress-info">
            <div class="progress">
                <span style="width: 100%;" class="progress-bar progress-bar-success purple-soft">
                    <span class="sr-only">56% progress</span>
                </span>
            </div>
            <div class="status">
                <div class="status-title"> progress </div>
                <div class="status-number"> 100% </div>
            </div>
        </div>
    </div>
    </a>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <a href="{{ route('backend.post.index') }}">
    <div class="dashboard-stat2 bordered">
        <div class="display">
            <div class="number">
                <h3 class="font-purple-soft">
                    <span data-counter="counterup" data-value="{{ count($post) }}"></span>
                </h3>
                <small>NEWS</small>
            </div>
            <div class="icon">
                <i class="icon-user"></i>
            </div>
        </div>
        <div class="progress-info">
            <div class="progress">
                <span style="width: 100%;" class="progress-bar progress-bar-success purple-soft">
                    <span class="sr-only">56% progress</span>
                </span>
            </div>
            <div class="status">
                <div class="status-title"> progress </div>
                <div class="status-number"> 100% </div>
            </div>
        </div>
    </div>
    </a>
</div>

@push('scripts')
@endpush
@endsection