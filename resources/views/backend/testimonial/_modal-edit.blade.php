<div class="modal fade bs-modal-lg" id="edit{{ $data->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <form action="{{ route('backend.testimonial.update',$data->id) }}"  method="post" enctype="multipart/form-data">
        <input name="_method" type="hidden" value="PATCH">
        {{ csrf_field() }}
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-laptop"></i> Edit Data</h4>
            </div>
            <div class="modal-body">
             <div class="portlet-body">
                    
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label class="control-label ">Name</label>
            <div class="col-md-12">
               <input type="text" class="form-control fa fa-file fileinput-exists" placeholder="Name" name="name" required value="{{ $data->name }}">
               </input>
               @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
            <label class="control-label">Company</label>
            <div class="col-md-12">
               <input type="text" class="form-control" name="company" placeholder="Company" value="{{ $data->company }}" required>
               </input>
               @if ($errors->has('company'))
                    <span class="help-block">
                        <strong>{{ $errors->first('company') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
            <label class="control-label">Description</label>
            <div class="col-md-12">
               <textarea class="form-control" id="summernote" name="description" required>{{ $data->description }}
               </textarea>
               @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>
        </div>

         <div class="form-group last{{ $errors->has('photo') ? ' has-error' : '' }}">
            <label class="control-label col-md-0">Photo</label>
            <div class="col-md-12">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                        <img src="{{ asset('/assets/img/testimonial/'.$data->photo.'') }}" alt="" /> </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                    <div>
                        <span class="btn default btn-file">
                            <span class="fileinput-new"> Select image </span>
                            <span class="fileinput-exists"> Change </span>
                            <input type="file" value="{{ old('photo', $data->photo)}}" name="photo"> </span>
                        <a href="javascript:;" class="btn blue fileinput-exists" data-dismiss="fileinput"> Remove </a>
                    </div>
                </div>
                @if ($errors->has('photo'))
                    <span class="help-block">
                        <strong>{{ $errors->first('photo') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        </div>
        </div>
            
            <div class="modal-footer">
                <button type="button" class="btn blue btn-outline" data-dismiss="modal">Close</button>
                <button type="submit" class="btn green">Save changes</button>
            </div>
        </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>