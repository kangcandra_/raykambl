@extends('layouts.backend')
@section('css')
<link href="/assets/backend/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/backend/pages/css/blog.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/backend/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/backend/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="/assets/backend/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
{!! Minify::stylesheet(array('/assets/backend/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')) !!}
{!! Minify::stylesheet(array('/assets/backend/global/plugins/bootstrap-summernote/summernote.css')) !!}
{!! Minify::stylesheet(array('/css/summernote.css')) !!}
<style>
    @font-face {
        src: url('{{ public_path('font/summernote.eot') }}');
        src: url('{{ public_path('font/summernote.woff') }}');
        src: url('{{ public_path('font/summernote.ttf') }}');
    }
</style>
@endsection

@section('js')
{!! Minify::javascript('/assets/backend/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')!!} 
{!! Minify::javascript('/js/summernote.min.js')!!}
{!! Minify::javascript('/js/summernote.js')!!}
@endsection

@section('title')
<i class="icon-settings"></i> <span class="title">Testimonial Management</span>
@endsection
@section('subtitle')
<i class="icon-settings"></i>
@endsection

@section('content')
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-bubble font-green-sharp"></i>
            <span class="caption-subject font-green-sharp sbold">Testimonials List</span>
        </div>
        <div class="actions">
            <a class="btn purple btn btn-circle sbold" data-toggle="modal" href="#tambah">  <i class="fa fa-plus"></i> Add </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
        </div>
    </div>
    {{-- Modal Tambah --}}
        @include('backend.testimonial._modal-add')
    {{-- End Modal --}}
<div class="blog-page blog-content-1">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                @foreach($testimonial as $data)
                <div class="col-sm-6">
                    <div class="blog-quote bordered blog-container">
                        <div class="blog-quote-label bg-green-jungle">
                            <i class="fa fa-quote-left"></i> {{ str_limit($data->description,57) }} </div>
                        <div class="blog-quote-avatar">
                            <a href="javascript:;">
                                <img src="{{ asset('/assets/img/testimonial/'.$data->photo.'') }}" />
                            </a>
                        </div>
                        <div class="blog-quote-author">
                            <h3 class="blog-title blog-quote-title">
                                <a href="javascript:;">{{ $data->name }}</a>
                            </h3>
                            <p class="blog-quote-desc"> {{ $data->company }}</p>
                            {!! Form::model($data, ['route' => ['backend.testimonial.destroy', $data->id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                <a class="btn yellow btn btn-circle sbold" data-toggle="modal" href="#edit{{$data->id}}">  <i class="fa fa-pencil"></i> Edit </a>
                                {!! Form::button('<i class="fa fa-trash">  Delete </i>', ['type' => 'submit','class'=>'btn red btn btn-circle sbold js-submit-confirm'])!!}
                                {!! Form::close()!!}
                        </div>
                    </div>
                </div>
                {{-- Edit Modal --}}
                 @include('backend.testimonial._modal-edit')
                {{-- End Edit Modal --}}
                @endforeach
                <center>{{ $testimonial->links() }}</center>
            </div>
        </div>
    </div>
</div>
</div>
<!-- END CONTENT BODY -->
            
@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#summernote').summernote({
             height: '400px',
             placeholder: 'Content Here....',
        });
        
    });
</script>
@endpush
@endsection