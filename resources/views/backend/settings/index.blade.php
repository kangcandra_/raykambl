@extends('layouts.backend')
@section('title')
<i class="icon-settings"></i> <span class="title">Profile Settings</span>
@endsection
@section('subtitle')
<i class="icon-settings"></i>
@endsection
@section('content')
<div class="row">           
<div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <i class="icon-share font-green-sharp"></i>
                <span class="caption-subject bold uppercase"> Profile Settings </span>
            </div>
             <div class="actions">
                <a class="btn green btn btn-circle sbold" data-toggle="modal" href="{{ url()->previous() }}">  <i class="fa fa-backward"></i> Back </a>
                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>   
            </div>
        </div>
            <div class="portlet-body">{{-- content --}}
                {!! Form::model(auth()->user(), ['url' => url('/admin/settings'),'method' => 'post']) !!}
                {{csrf_field()}}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    {!! Form::label('name', 'Name', ['class'=>'control-label']) !!}
                    {!! Form::text('name', null, ['class'=>'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    {!! Form::label('email', 'Email', ['class'=>'control-label']) !!}
                    {!! Form::email('email', null, ['class'=>'form-control']) !!}
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    {!! Form::label('password', 'Password lama', ['class'=>'control-label']) !!}
                    {!! Form::password('password', ['class'=>'form-control']) !!}
                    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
                    {!! Form::label('new_password', 'Password baru', ['class'=>'control-label']) !!}
                    {!! Form::password('new_password', ['class'=>'form-control']) !!}
                    {!! $errors->first('new_password', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group{{ $errors->has('new_password_confirmation') ? ' has-error' : '' }}">
                    {!! Form::label('new_password_confirmation', 'Konfirmasi password baru', ['class'=>'control-label']) !!}
                    {!! Form::password('new_password_confirmation', ['class'=>'form-control']) !!}
                    {!! $errors->first('new_password_confirmation', '<p class="help-block">:message</p>') !!}
                    </div>


                    <div class="form-group">
                    <button  type="submit" class="btn blue btn-outline">Save</button>
                    <button type="reset"  class="btn green btn-outline">Reset</button>
                    </div>

                {!! Form::close() !!}
            </div>{{-- endcontent --}}
           
    </div><!-- END Portlet PORTLET-->
</div>     
</div>
@endsection