@extends('layouts.backend')
@section('css')
{!! Minify::stylesheet(array('/assets/backend/pages/css/blog.min.css')) !!}
{!! Minify::stylesheet(array('/assets/backend/global/plugins/cubeportfolio/css/cubeportfolio.css')) !!}
{!! Minify::stylesheet(array('/assets/backend/pages/css/portfolio.min.css')) !!}
{!! Minify::stylesheet(array('/assets/backend/global/plugins/select2/css/select2.min.css')) !!}
{!! Minify::stylesheet(array('/assets/backend/global/plugins/select2/css/select2-bootstrap.min.css')) !!}
{!! Minify::stylesheet(array('/assets/backend/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')) !!}

@endsection

@section('js')
{!! Minify::javascript('/assets/backend/global/scripts/app.min.js')!!}
{!! Minify::javascript('/assets/backend/global/plugins/jquery-ui/jquery-ui.min.js')!!}
{!! Minify::javascript('/assets/backend/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js')!!}
{!! Minify::javascript('/assets/backend/pages/scripts/portfolio-1.min.js')!!}

@endsection

@section('title')
<i class="icon-settings"></i> <span class="title">Product Management</span>
@endsection
@section('subtitle')
<i class="icon-settings"></i>
@endsection

@section('content')
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-bubble font-green-sharp"></i>
            <span class="caption-subject font-green-sharp sbold">Product List</span>
        </div>
       <div class="actions">
            <a class="btn purple btn btn-circle sbold" data-toggle="modal" href="{{ route('backend.product.create') }}">  <i class="fa fa-plus"></i> Add </a>
        </div>
    </div>
    {{-- @include('backend.product._modal-add') --}}
    {{-- Post List --}}
    <div class="portfolio-content portfolio-1">
        <div id="js-filters-juicy-projects" class="cbp-l-filters-button">
            <div data-filter="*" class="cbp-filter-item-active cbp-filter-item btn dark btn-outline uppercase"> All
                <div class="cbp-filter-counter"></div>
            </div>
            @foreach($categories as $data)
            @if($data->Product->count() > 0)
            <div data-filter=".product{{ $data->id }}" class="cbp-filter-item-active cbp-filter-item btn dark btn-outline uppercase">{{ $data->name }} 
                <div class="cbp-filter-counter"></div>
            </div>
            @endif
            @endforeach
        </div>
        <div id="js-grid-juicy-projects" class="cbp">
        @foreach($products as $data)
            <div class="cbp-item product{{ $data->ProductCategory->id }}"  style="width: 312px; left: 0px; top: 1202px;">
                <div class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <a href="{{ asset('/assets/img/product/'.$data->photo.'') }}" class="cbp-lightbox" data-title="{{ $data->title }}<br>by Admin"><img style="width: 340px; height: 212px;" src="{{ asset('assets/img/product/'.$data->photo.'') }}" alt=""></a> </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignCenter">
                            <div class="cbp-l-caption-body">
                               
                                {!! Form::model($data, ['route' => ['backend.product.destroy', $data->id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                <a class="cbp-singlePage btn blue btn btn-circle sbold" href="{{route('backend.product.show',$data->id)}}">  <i class="fa fa-search"></i> Show </a>
                                <a class="btn yellow btn btn-circle sbold" data-toggle="modal" href="{{route('backend.product.edit',$data->id)}}">  <i class="fa fa-pencil"></i> Edit </a>
                                {!! Form::button('<i class="fa fa-trash">  Delete </i>', ['type' => 'submit','class'=>'btn red btn btn-circle sbold js-submit-confirm'])!!}
                                {!! Form::close()!!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cbp-l-grid-projects-title uppercase text-center uppercase text-center">{{ str_limit($data->name,30) }}...</div>
                <div class="cbp-l-grid-projects-desc uppercase text-center uppercase text-center">{{-- {{$data->Category->name}} --}}</div>
            </div>
        @endforeach
        </div>
        {{-- Pagination --}}
        <center>{{ $products->links() }}</center>
        {{-- EndPagination --}}
    </div>
    {{-- End Post List --}}
</div>

@push('scripts')
@endpush
@endsection