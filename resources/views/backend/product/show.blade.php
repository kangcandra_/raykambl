<div class="portfolio-content">
    <div class="cbp-l-project-title">{{ $product->name }}</div>
    <div class="cbp-l-project-subtitle"></div>
    <center><img src="{{ asset('assets/img/product/'.$product->photo.'') }}" style="width: 1000px; height: 800px;" alt=""></center>
    <div class="cbp-l-project-container">
        <div class="cbp-l-project-desc">
            <div class="cbp-l-project-desc-title">
                <span>product Description</span>
            </div>
            <div class="cbp-l-project-desc-text"><p>{!! $product->description !!}</p></div>
        </div>
        <div class="cbp-l-project-details">
            <div class="cbp-l-project-details-title">
                <span>product Details</span>
            </div>
            <ul class="cbp-l-project-details-list">
                <li><strong>Price</strong>Rp. {{ number_format($product->price,2,",",".") }}</li>
                <li><strong>Date</strong>{{ $product->created_at->diffForHumans() }}</li>
                <li><strong>Categories</strong>{{ $product->ProductCategory->name }}</li>
                <li>
                    <strong>Tags</strong>@foreach($product->ProductTag as $data){{ $data->name }}, @endforeach</li>
            </ul>
            <a href="/product/{{ $product->slug }}" target="_blank" class="cbp-l-project-details-visit btn red uppercase"><i class="fa fa-rocket"></i> visit the site</a>
        </div>
    </div>
    <br>
    <br>
    <br> </div>