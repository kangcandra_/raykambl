<div class="portfolio-content">
    <div class="cbp-l-project-title">{{ $portofolio->title }}</div>
   
        <center><img src="{{ asset('assets/img/portofolio/'.$portofolio->photo.'') }}" style="width: 1000px; height: 800px;" alt=""></center>
    <div class="cbp-l-project-container">
        <div class="cbp-l-project-desc">
            <div class="cbp-l-project-desc-title">
                <span>Portofolio Description</span>
            </div>
            <div class="cbp-l-project-desc-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium, cumque, earum blanditiis incidunt minus commodi consequatur provident quae. Nihil, alias, vel consequatur ab aliquam aspernatur optio harum facilis excepturi mollitia autem
                voluptas cum ex veniam numquam quia repudiandae in iure. Assumenda, vel provident molestiae perferendis officia commodi asperiores earum sapiente inventore quam deleniti mollitia consequatur expedita quaerat natus praesentium beatae aut
                ipsa non ex ullam atque suscipit ut dignissimos magnam!</div>
        </div>
        <div class="cbp-l-project-details">
            <div class="cbp-l-project-details-title">
                <span>Portofolio Details</span>
            </div>
            <ul class="cbp-l-project-details-list">
                <li>
                    <strong>Categories</strong>{{ $portofolio->PortofolioCategory->name }}</li>
            </ul>
            <a href="{{ url('/portofolio') }}" target="_blank" class="cbp-l-project-details-visit btn red uppercase"><i class="fa fa-rocket"></i> visit the site</a>
        </div>
    </div>
    <br>
    <br>
    <br> </div>