<div class="modal fade bs-modal-lg" id="edit{{ $data->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <form action="{{ route('backend.product_tag.update',$data->id) }}"  method="post">
        <input name="_method" type="hidden" value="PATCH">
        {{ csrf_field() }}
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-laptop"></i> Edit Data</h4>
            </div>
            <div class="modal-body">
             <div class="portlet-body">
                    
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label class="control-label ">Tag Name</label>
            <div class="col-md-12">
               <input type="text" class="form-control fa fa-file fileinput-exists" placeholder="Tag" name="name" required value="{{ $data->name }}">
               </input>
               @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        </div>
        </div>
            
            <div class="modal-footer">
                <button type="button" class="btn blue btn-outline" data-dismiss="modal">Close</button>
                <button type="submit" class="btn green">Save changes</button>
            </div>
        </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>