@extends('layouts.backend')
@section('css')
{!! Minify::stylesheet(array('/assets/backend/pages/css/blog.min.css')) !!}
{!! Minify::stylesheet(array('/assets/backend/global/plugins/cubeportfolio/css/cubeportfolio.css')) !!}
{!! Minify::stylesheet(array('/assets/backend/pages/css/portfolio.min.css')) !!}
@endsection

@section('js')
{!! Minify::javascript('/assets/backend/global/scripts/app.min.js')!!}
{!! Minify::javascript('/assets/backend/global/plugins/jquery-ui/jquery-ui.min.js')!!}
{!! Minify::javascript('/assets/backend/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js')!!}
{!! Minify::javascript('/assets/backend/pages/scripts/portfolio-1.min.js')!!}
@endsection

@section('title')
<i class="icon-settings"></i> <span class="title">Blog Management</span>
@endsection
@section('subtitle')
<i class="icon-settings"></i>
@endsection

@section('content')
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-bubble font-green-sharp"></i>
            <span class="caption-subject font-green-sharp sbold">Blog List</span>
        </div>
       <div class="actions">
            <a class="btn purple btn btn-circle sbold" href="{{ route('backend.post.create') }}">  <i class="fa fa-plus"></i> Add </a>
        </div>
        
    </div>
    {{-- Post List --}}
    <div class="portfolio-content portfolio-1">
        <div id="js-filters-juicy-projects" class="cbp-l-filters-button">
            <div data-filter="*" class="cbp-filter-item-active cbp-filter-item btn dark btn-outline uppercase"> All
                <div class="cbp-filter-counter"></div>
            </div>
            @foreach($categories as $data)
            @if($data->Post->count() > 0)
            <div data-filter=".post{{ $data->id }}" class="cbp-filter-item-active cbp-filter-item btn dark btn-outline uppercase">{{ $data->name }} 
                <div class="cbp-filter-counter"></div>
            </div>
            @endif
            @endforeach
        </div>
        <div id="js-grid-juicy-projects" class="cbp">
        @foreach($posts as $data)
            <div class="cbp-item post{{ $data->Category->id }}"  style="width: 312px; left: 0px; top: 1202px;">
                <div class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <a href="{{ asset('/assets/img/post/'.$data->photo.'') }}" class="cbp-lightbox" data-title="{{ $data->title }}<br>by {{ $data->User->name }}"><img style="width: 340px; height: 212px;" src="{{ asset('assets/img/post/'.$data->photo.'') }}" alt=""></a> </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignCenter">
                            <div class="cbp-l-caption-body">
                               
                                {!! Form::model($data, ['route' => ['backend.post.destroy', $data->id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                <a class="cbp-singlePage btn blue btn btn-circle sbold" href="{{route('backend.post.show',$data->id)}}">  <i class="fa fa-search"></i> Show </a>
                                <a class="btn yellow btn btn-circle sbold" href="{{route('backend.post.edit',$data->id)}}">  <i class="fa fa-pencil"></i> Edit </a>
                                {!! Form::button('<i class="fa fa-trash">  Delete </i>', ['type' => 'submit','class'=>'btn red btn btn-circle sbold js-submit-confirm'])!!}
                                {!! Form::close()!!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cbp-l-grid-projects-title uppercase text-center uppercase text-center">{{ str_limit($data->title,30) }}...</div>
                <div class="cbp-l-grid-projects-desc uppercase text-center uppercase text-center">{{$data->Category->name}}</div>
            </div>
        @endforeach
        </div>
        {{-- Pagination --}}
        <center>{{ $posts->links() }}</center>
        {{-- EndPagination --}}
    </div>
    {{-- End Post List --}}
</div>

@push('scripts')
@endpush
@endsection