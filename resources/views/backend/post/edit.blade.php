@extends('layouts.backend')
@section('css')
{!! Minify::stylesheet(array('/assets/backend/global/plugins/select2/css/select2.min.css')) !!}
       {!! Minify::stylesheet(array('/assets/backend/global/plugins/select2/css/select2-bootstrap.min.css')) !!}
{!! Minify::stylesheet(array('/assets/backend/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css')) !!}
{!! Minify::stylesheet(array('/assets/backend/global/plugins/bootstrap-summernote/summernote.css')) !!}
{!! Minify::stylesheet(array('/css/summernote.css')) !!}
{!! Minify::stylesheet(array('/assets/backend/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')) !!}
<style>
@font-face {
    src: url('{{ public_path('font/summernote.eot') }}');
    src: url('{{ public_path('font/summernote.woff') }}');
    src: url('{{ public_path('font/summernote.ttf') }}');
}
</style>

@endsection
@section('js')
{!! Minify::javascript('/assets/backend/global/plugins/select2/js/select2.full.min.js')!!}
{!! Minify::javascript('/assets/backend/pages/scripts/components-select2.min.js')!!}
{!! Minify::javascript('/js/summernote.min.js')!!}
{!! Minify::javascript('/js/summernote.js')!!}
{!! Minify::javascript('/assets/backend/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')!!} 
@endsection

@section('title')
<i class="icon-settings"></i> <span class="title">Blog Management</span>
@endsection
@section('sub')
Dashboard
@endsection

@section('content')
<div class="row">           
    <div class="col-md-12">
                <!-- BEGIN Portlet PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <i class="icon-share font-green-sharp"></i>
                    <span class="caption-subject bold uppercase"> Create Blog </span>
                </div>
                 <div class="actions">
                    <a class="btn green btn btn-circle sbold" data-toggle="modal" href="{{ url()->previous() }}">  <i class="fa fa-backward"></i> Back </a>
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>   
                </div>
            </div>
	            <div class="portlet-body">{{-- content --}}
	             	<form action="{{route('backend.post.update',$post->id)}}" method="post" enctype="multipart/form-data">
                    <input name="_method" type="hidden" value="PATCH">
                    {{csrf_field()}}
	             		<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
         				<label class="control-label">Title</label>
         				<input type="text" name="title" class="form-control" required placeholder="Title" value="{{ $post->title }}">
                        @if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
         				</div>

                        <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                        <label class="control-label">Content</label>
                        <textarea id="summernote" name="content" class="form-control">{!! $post->content !!}</textarea>
                        @if ($errors->has('content'))
                            <span class="help-block">
                                <strong>{{ $errors->first('content') }}</strong>
                            </span>
                        @endif
                        </div>


                         <div class="form-group last{{ $errors->has('photo') ? ' has-error' : '' }}">
                            <label class="control-label col-md-0">Photo</label>
                            <div class="col-md-12">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                        <img src="{{ asset('/assets/img/post/'.$post->photo.'') }}" alt="" /> </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                    <div>
                                        <span class="btn default btn-file">
                                            <span class="fileinput-new"> Select image </span>
                                            <span class="fileinput-exists"> Change </span>
                                            <input type="file" name="photo" value="{{ $post->photo }}"> </span>
                                        <a href="javascript:;" class="btn blue fileinput-exists"  data-dismiss="fileinput"> Remove </a>
                                    </div>
                                </div>
                                @if ($errors->has('photo'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('photo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        
         				<div class="form-group{{ $errors->has('categories_id') ? ' has-error' : '' }}">
         				<label class="control-label">Category</label>
         				<select name="categories_id" id="single" class="form-control select2" required="">
                            <option></option>
                            @foreach($categories as $category)
                            <option value="{{$category->id}}" {{ $selectedCategory == $category->id ? 'selected="selected"' : '' }}>{{$category->name}}</option>
                            @endforeach

                        </select>
                        @if ($errors->has('categories_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('categories_id') }}</strong>
                            </span>
                        @endif
         				</div>

                 <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
                    <label for="multiple" class="control-label">Tags</label>
                    <select id="multiple" name="tags[]" class="form-control select2-multiple" multiple required>
                    @foreach($tags as $tag)    
                    <option value="{{ $tag->id}}"{{ (in_array($tag->id, $selected)) ? ' selected="selected"' : '' }}>{{$tag->name}}</option>
                    @endforeach
                    </select>
                    @if ($errors->has('tags'))
                    <span class="help-block">
                        <strong>{{ $errors->first('tags') }}</strong>
                    </span>
                @endif
                </div>


         				<div class="form-group">
         				<button  type="submit" class="btn blue btn-outline">Save</button>
         				<button type="reset"  class="btn green btn-outline">Reset</button>
         				</div>

	             	</form>
	        	</div>{{-- endcontent --}}
               
        </div><!-- END Portlet PORTLET-->
    </div>     
</div>
@push('scripts')
        <script>
    $(document).ready(function() {
        $('#summernote').summernote({
             height: '400px',
             placeholder: 'Content Here....',
        });
        
    });
  </script>
  <script>
      $('#multiple').select2();
      $('#single').select2();
  </script>
        @endpush
@endsection
