<div class="portfolio-content">
    <div class="cbp-l-project-title">{{ $post->title }}</div>
    <div class="cbp-l-project-subtitle"></div>
    <center><img src="{{ asset('assets/img/post/'.$post->photo.'') }}" style="width: 1000px; height: 800px;" alt=""></center>
    <div class="cbp-l-project-container">
        <div class="cbp-l-project-desc">
            <div class="cbp-l-project-desc-title">
                <span>Post Description</span>
            </div>
            <div class="cbp-l-project-desc-text"><p>{!! $post->content !!}</p></div>
        </div>
        <div class="cbp-l-project-details">
            <div class="cbp-l-project-details-title">
                <span>Post Details</span>
            </div>
            <ul class="cbp-l-project-details-list">
                <li>
                    <strong>Author</strong>{{ $post->User->name }}</li>
                <li>
                    <strong>Date</strong>{{ $post->created_at->format('d M Y') }}</li>
                <li>
                    <strong>Categories</strong>{{ $post->Category->name }}</li>
                <li>
                    <strong>Tags</strong>@foreach($post->Tag as $key){{ $key->name }}, @endforeach</li>
            </ul>
            <a href="/blog/{{ $post->slug }}" target="_blank" class="cbp-l-project-details-visit btn red uppercase"><i class="fa fa-rocket"></i> visit the site</a>
        </div>
    </div>
    <br>
    <br>
    <br> </div>