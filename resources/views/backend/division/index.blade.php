@extends('layouts.backend')
@section('css')
@endsection

@section('js')
@endsection

@section('title')
<i class="icon-settings"></i> <span class="title">Team Management</span>
@endsection
@section('subtitle')
<i class="icon-settings"></i>
@endsection

@section('content')
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-bubble font-green-sharp"></i>
            <span class="caption-subject font-green-sharp sbold">Team Division</span>
        </div>
       <div class="actions">
            <a class="btn purple btn btn-circle sbold" data-toggle="modal" href="#tambah">  <i class="fa fa-plus"></i> Add </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
        </div>
        {{-- Modal Tambah --}}
        @include('backend.division._modal-add')
        {{-- End Modal --}}
    </div>
    {{-- Table --}}
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th> No </th>
                    <th> Name </th>
                    <th colspan="3"> Action </th>
                </tr>
            </thead>
            <tbody>
            @php $no=1; @endphp
            @foreach($division as $data)
                <tr>
                    <td> {{ $no++ }} </td>
                    <td> {{ $data->name }} </td>
                    <td><a class="btn yellow btn btn-circle sbold" data-toggle="modal" href="#edit{{$data->id}}">  <i class="fa fa-pencil"></i> Edit </a>
                    </td>
                    <td>
                        {!! Form::model($data, ['route' => ['backend.team_division.destroy', $data->id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                        {!! Form::button('<i class="fa fa-trash">  Delete </i>', ['type' => 'submit','class'=>'btn red btn btn-circle sbold js-submit-confirm'])!!}
                        {!! Form::close()!!}
                    </td>
                    <td></td>
                </tr>
                {{-- Edit Modal --}}
                 @include('backend.division._modal-edit')
                {{-- End Edit Modal --}}
            @endforeach
            </tbody>
        </table>
        <center>{{ $division->render() }}</center>
    </div>
    {{-- End Table --}}
</div>

@push('scripts')
@endpush
@endsection