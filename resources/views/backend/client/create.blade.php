@extends('layouts.backend')
@section('css')
{!! Minify::stylesheet(array('/assets/backend/global/plugins/select2/css/select2.min.css')) !!}
{!! Minify::stylesheet(array('/assets/backend/global/plugins/select2/css/select2-bootstrap.min.css')) !!}
{!! Minify::stylesheet(array('/assets/backend/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css')) !!}
{!! Minify::stylesheet(array('/assets/backend/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')) !!}
{!! Minify::stylesheet(array('/assets/backend/global/plugins/bootstrap-summernote/summernote.css')) !!}
{!! Minify::stylesheet(array('/css/summernote.css')) !!}
<style>
    @font-face {
        src: url('{{ public_path('font/summernote.eot') }}');
        src: url('{{ public_path('font/summernote.woff') }}');
        src: url('{{ public_path('font/summernote.ttf') }}');
    }
</style>
@endsection
@section('js')
{!! Minify::javascript('/assets/backend/global/plugins/select2/js/select2.full.min.js')!!}
{!! Minify::javascript('/assets/backend/pages/scripts/components-select2.min.js')!!}
{!! Minify::javascript('/assets/backend/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')!!} 
{!! Minify::javascript('/js/summernote.min.js')!!}
{!! Minify::javascript('/js/summernote.js')!!} 
@endsection

@section('title')
<i class="icon-settings"></i> <span class="title">Client Management</span>
@endsection
@section('sub')
Dashboard
@endsection

@section('content')
<div class="row">           
    <div class="col-md-12">
                <!-- BEGIN Portlet PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <i class="icon-share font-green-sharp"></i>
                    <span class="caption-subject bold uppercase"> Create client </span>
                </div>
                 <div class="actions">
                    <a class="btn green btn btn-circle sbold" data-toggle="modal" href="{{ url()->previous() }}">  <i class="fa fa-backward"></i> Back </a>
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>   
                </div>
            </div>
	            <div class="portlet-body">{{-- content --}}
	             	<form action="{{route('backend.client.store')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
	             	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
         				<label class="control-label">Title</label>
         				<input type="text" name="name" class="form-control" placeholder="Name" required>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
         				</div>

                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                <label class="control-label">Description</label>
                <textarea class="form-control" id="summernote" name="description" placeholder="Description" required></textarea>
                        @if($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                </div>

                <div class="form-group last{{ $errors->has('photo') ? ' has-error' : '' }}">
                    <label class="control-label col-md-0">Photo</label>
                    <div class="col-md-12">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new"> Select image </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" name="photo"> </span>
                                <a href="javascript:;" class="btn blue fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                        </div>
                        @if ($errors->has('photo'))
                            <span class="help-block">
                                <strong>{{ $errors->first('photo') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                        
         				<div class="form-group{{ $errors->has('clientcategories_id') ? ' has-error' : '' }}">
         				<label class="control-label">Category</label>
         				<select name="clientcategories_id" id="single" class="form-control select2" required="">
                            <option></option>
                            @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('clientcategories_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('clientcategories_id') }}</strong>
                            </span>
                        @endif
         				</div>
                
                <div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
                <label class="control-label">Link</label>
                <input type="text" name="link" class="form-control" placeholder="https://yourwebsite.com" required>
                        @if ($errors->has('link'))
                            <span class="help-block">
                                <strong>{{ $errors->first('link') }}</strong>
                            </span>
                        @endif
                </div>

         				<div class="form-group">
         				<button  type="submit" class="btn blue btn-outline">Save</button>
         				<button type="reset"  class="btn green btn-outline">Reset</button>
         				</div>

	             	</form>
	        	</div>{{-- endcontent --}}
               
        </div><!-- END Portlet PORTLET-->
    </div>     
</div>
@push('scripts')
<script>
        $(document).ready(function() {
            $('#summernote').summernote({
                 height: '400px',
                 placeholder: 'Content Here....',
            });
            
        });
      $('#multiple').select2();
      $('#single').select2();
      $('#multiple').select2();
      $('#single').select2();
</script>
        @endpush
@endsection
