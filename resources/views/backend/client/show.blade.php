<div class="portfolio-content">
    <div class="cbp-l-project-title">{{ $client->name }}</div>
   
        <center><img src="{{ asset('assets/img/client/'.$client->photo.'') }}" style="width: 1000px; height: 800px;" alt=""></center>
    <div class="cbp-l-project-container">
        <div class="cbp-l-project-desc">
            <div class="cbp-l-project-desc-title">
                <span>client Description</span>
            </div>
            <div class="cbp-l-project-desc-text"><p>{!! $client->description !!}</p></div>
        </div>
        <div class="cbp-l-project-details">
            <div class="cbp-l-project-details-title">
                <span>client Details</span>
            </div>
            <ul class="cbp-l-project-details-list">
                <li>
                    <strong>Categories</strong>{{ $client->ClientCategory->name }}</li>
            </ul>
            <a href="{{ url('/client') }}" target="_blank" class="cbp-l-project-details-visit btn red uppercase"><i class="fa fa-rocket"></i> visit the site</a>
        </div>
    </div>
    <br>
    <br>
    <br> </div>