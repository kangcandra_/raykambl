@extends('layouts.backend')
@section('css')
    {!! Minify::stylesheet(['/assets/backend/pages/css/blog.min.css']) !!}
    {!! Minify::stylesheet(['/assets/backend/global/plugins/cubeportfolio/css/cubeportfolio.css']) !!}
    {!! Minify::stylesheet(['/assets/backend/pages/css/portfolio.min.css']) !!}
@endsection

@section('js')
    {!! Minify::javascript('/assets/backend/global/scripts/app.min.js') !!}
    {!! Minify::javascript('/assets/backend/global/plugins/jquery-ui/jquery-ui.min.js') !!}
    {!! Minify::javascript('/assets/backend/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js') !!}
    {!! Minify::javascript('/assets/backend/pages/scripts/portfolio-1.min.js') !!}
@endsection

@section('title')
    <i class="icon-settings"></i> <span class="title">Client Management</span>
@endsection
@section('subtitle')
    <small><i class="icon-settings"></i></small>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-bubble font-green-sharp"></i>
                <span class="caption-subject font-green-sharp sbold">Client List</span>
            </div>
            <div class="actions">
                <a class="btn purple btn btn-circle sbold" href="{{ route('backend.client.create') }}"> <i
                        class="fa fa-plus"></i> Add </a>
            </div>

        </div>
        {{-- Post List --}}
        <div class="portfolio-content portfolio-1">
            <div id="js-filters-juicy-projects" class="cbp-l-filters-button">
                <div data-filter="*" class="cbp-filter-item-active cbp-filter-item btn dark btn-outline uppercase"> All
                    <div class="cbp-filter-counter"></div>
                </div>
                @foreach ($categories as $data)
                    @if ($data->Client->count() > 0)
                        <div data-filter=".client{{ $data->id }}"
                            class="cbp-filter-item-active cbp-filter-item btn dark btn-outline uppercase">
                            {{ $data->name }}
                            <div class="cbp-filter-counter"></div>
                        </div>
                    @endif
                @endforeach
            </div>
            <div id="js-grid-juicy-projects" class="cbp">
                @foreach ($client as $data)
                    <div class="cbp-item client{{ $data->ClientCategory->id }}"
                        style="width: 312px; left: 0px; top: 1202px;">
                        <div class="cbp-caption">
                            <div class="cbp-caption-defaultWrap">
                                <a href="{{ asset('assets/img/client/' . $data->photo . '') }}"
                                    style="height: 385px; width:145px;" class="cbp-lightbox"
                                    data-title="{{ $data->name }}"><img style="width: 340px; height: 212px;"
                                        src="{{ asset('assets/img/client/' . $data->photo . '') }}" alt=""></a>
                            </div>
                            <div class="cbp-caption-activeWrap">
                                <div class="cbp-l-caption-alignCenter">
                                    <div class="cbp-l-caption-body">

                                        {!! Form::model($data, ['route' => ['backend.client.destroy', $data->id], 'method' => 'delete', 'class' => 'form-inline']) !!}
                                        <input type="hidden" value="{{ csrf_token() }}" name="">
                                        <a class="cbp-singlePage btn blue btn btn-circle sbold"
                                            href="{{ route('backend.client.show', $data->id) }}"> <i
                                                class="fa fa-search"></i> Show </a>
                                        <a class="btn yellow btn btn-circle sbold"
                                            href="{{ route('backend.client.edit', $data->id) }}"> <i
                                                class="fa fa-pencil"></i> Edit </a>
                                        {!! Form::button('<i class="fa fa-trash">  Delete </i>', ['type' => 'submit', 'class' => 'btn red btn btn-circle sbold js-submit-confirm']) !!}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cbp-l-grid-projects-title uppercase text-center uppercase text-center">
                            {{ str_limit($data->title, 30) }}</div>
                        <div class="cbp-l-grid-projects-desc uppercase text-center uppercase text-center">
                            {{ $data->ClientCategory->name }}</div>
                    </div>
                @endforeach
            </div>
            {{-- Pagination --}}
            <center>{{ $client->links() }}</center>
            {{-- EndPagination --}}
        </div>
        {{-- End Post List --}}
    </div>

    @push('scripts')
    @endpush
@endsection
