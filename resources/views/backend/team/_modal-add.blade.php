<div class="modal fade bs-modal-lg" id="tambah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <form action="{{ route('backend.team.store') }}"  method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-laptop"></i> Add Data</h4>
            </div>
        <div class="modal-body">
        <div class="portlet-body">
                    
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="control-label">Name</label>
                    <div class="col-md-12">
                       <input type="text" class="form-control" name="name" placeholder="Name" required>
                       </input>
                       @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('divisions_id') ? ' has-error' : '' }}">
                    <label class="control-label">Division</label>
                    <div class="col-md-12">
                    <select name="divisions_id" id="single" class="form-control select2" required="">
                        <option></option>
                        @foreach($division as $data)
                        <option value="{{$data->id}}">{{$data->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('divisions_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('divisions_id') }}</strong>
                        </span>
                    @endif
                    </div>
                </div>
                <div class="form-group last{{ $errors->has('photo') ? ' has-error' : '' }}">
                    <label class="control-label col-md-0">Photo</label>
                    <div class="col-md-12">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new"> Select image </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" name="photo"> </span>
                                <a href="javascript:;" class="btn blue fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                        </div>
                        @if ($errors->has('photo'))
                            <span class="help-block">
                                <strong>{{ $errors->first('photo') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('yt') ? ' has-error' : '' }}">
                        <label class="control-label">Youtube (ID)</label>
                        <div class="col-md-12">
                        <input type="text" name="yt" class="form-control" placeholder="example : 1000012020">
                        @if ($errors->has('yt'))
                            <span class="help-block">
                                <strong>{{ $errors->first('yt') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                 <div class="form-group{{ $errors->has('fb') ? ' has-error' : '' }}">
                        <label class="control-label">Facebook (Username)</label>
                        <div class="col-md-12">
                        <input type="text" name="fb" class="form-control" placeholder="Username">
                        @if ($errors->has('fb'))
                            <span class="help-block">
                                <strong>{{ $errors->first('fb') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                 <div class="form-group{{ $errors->has('gp') ? ' has-error' : '' }}">
                        <label class="control-label">Google Plus (Username / ID)</label>
                        <div class="col-md-12">
                        <input type="text" name="gp" class="form-control" placeholder="+Username or ID">
                        @if ($errors->has('gp'))
                            <span class="help-block">
                                <strong>{{ $errors->first('gp') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                 <div class="form-group{{ $errors->has('tw') ? ' has-error' : '' }}">
                        <label class="control-label">Twitter (Username)</label>
                        <div class="col-md-12">
                        <input type="text" name="tw" class="form-control" placeholder="Username">
                        @if ($errors->has('tw'))
                            <span class="help-block">
                                <strong>{{ $errors->first('tw') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
        </div>
        </div>
            
            <div class="modal-footer">
                <button type="button" class="btn blue btn-outline" data-dismiss="modal">Close</button>
                <button type="submit" class="btn green">Save changes</button>
            </div>
        </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>