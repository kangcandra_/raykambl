@extends('layouts.backend')
@section('css')
<link href="/assets/backend/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/backend/pages/css/blog.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/backend/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/backend/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="/assets/backend/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
{!! Minify::stylesheet(array('/assets/backend/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')) !!}
{!! Minify::stylesheet(array('/assets/backend/global/plugins/select2/css/select2.min.css')) !!}
{!! Minify::stylesheet(array('/assets/backend/global/plugins/select2/css/select2-bootstrap.min.css')) !!}
<link href="/assets/backend/global/plugins/socicon/socicon.css" rel="stylesheet" type="text/css">

@endsection

@section('js')
@section('js')
{!! Minify::javascript('/assets/backend/global/plugins/select2/js/select2.full.min.js')!!}
{!! Minify::javascript('/assets/backend/pages/scripts/components-select2.min.js')!!}
{!! Minify::javascript('/assets/backend/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')!!} 
@endsection

@section('title')
<i class="icon-settings"></i> <span class="title">Team Management</span>
@endsection
@section('subtitle')
<i class="icon-settings"></i>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-green"></i>
                    <span class="caption-subject font-green bold uppercase">Team</span>
                </div>
                <div class="actions">
                    <a class="btn purple btn btn-circle sbold" data-toggle="modal" href="#tambah">  <i class="fa fa-plus"></i> Add </a>
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
                </div>
                 {{-- Modal Tambah --}}
                    @include('backend.team._modal-add')
                {{-- End Modal --}}
            </div>
            <div class="portlet-body">
                <div class="mt-element-card mt-element-overlay">
                    <div class="row">
                        @foreach($team as $data)
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <div class="mt-card-item">
                                <div class="mt-card-avatar mt-overlay-1" height="294px;">
                                    <img src="{{ asset('/assets/img/team/'.$data->photo.'') }}" style="width: 294px; height: 294px;" />
                                    <div class="mt-overlay">
                                        <ul class="mt-info">
                                            <li>
                                                <a class="btn default btn-outline sbold" data-toggle="modal" href="#edit{{$data->id}}">
                                                    <i class="icon-pencil"></i>
                                                </a>
                                            </li>
                                            <li>
                                                {!! Form::model($data, ['route' => ['backend.team.destroy', $data->id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                                <button class="btn default btn-outline js-submit-confirm" type="submit">
                                                    <i class="icon-trash"></i>
                                                </button>
                                                {!! Form::close()!!}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="mt-card-content">
                                    <h3 class="mt-card-name">{{ $data->name }}</h3>
                                    <p class="mt-card-desc font-grey-mint">{{ $data->Division->name }}</p>
                                    <div class="mt-card-social">
                                        <ul>
                                            @if($data->fb != null)
                                            <li>
                                                <a href="https://www.facebook.com/{{ $data->fb }}" class="socicon-btn socicon-facebook tooltips" data-original-title="Facebook"></a>
                                            </li>
                                            @endif
                                            @if($data->tw != null)
                                            <li>
                                                <a href="https://www.twitter.com/{{ $data->tw }}" class="socicon-btn socicon-twitter tooltips" data-original-title="Twitter"></a>
                                            </li>
                                            @endif
                                            @if($data->gp != null)
                                            <li>
                                                <a href="https://www.plus.google.com/{{ $data->gp }}" class="socicon-btn socicon-google tooltips" data-original-title="Google Plus"></a>
                                            </li>
                                            @endif
                                            @if($data->yt != null)
                                            <li>
                                                <a href="https://www.youtube.com/channel/{{ $data->yt }}" class="socicon-btn socicon-youtube tooltips" data-original-title="Youtube"></a>
                                            </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- Modal Edit --}}
                            @include('backend.team._modal-edit')
                        {{-- End Modal --}}
                        @endforeach
                        <center>{{ $team->links() }}</center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
            
@push('scripts')

@endpush
@endsection