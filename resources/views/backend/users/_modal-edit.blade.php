<div class="modal fade bs-modal-lg" id="edit{{ $data->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <form action="{{ route('backend.users.update',$data->id) }}"  method="post">
        <input name="_method" type="hidden" value="PATCH">
        {{ csrf_field() }}
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-laptop"></i> Edit Data</h4>
            </div>
            <div class="modal-body">
             <div class="portlet-body">
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label class="control-label">Name</label>
            <div class="col-md-12">
               <input type="text" class="form-control" name="name" required placeholder="Name" value="{{ $data->name }}">
               </input>
               @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label class="control-label ">Email Address</label>
            <div class="col-md-12">
               <input type="email" class="form-control" name="email" required placeholder="Email" value="{{ $data->email }}">
               </input>
               @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="control-label ">Password</label>
            <div class="col-md-12">
               <input type="password" class="form-control" name="password" placeholder="Password" required>
               </input>
               @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('display_name') ? ' has-error' : '' }}">
            <label class="control-label ">Password Confirmation</label>
            <div class="col-md-12">
               <input type="password" class="form-control" name="password_confirmation" placeholder="Password Confirmation" required>
               </input>
            </div>
        </div>
         <div class="form-group{{ $errors->has('permission') ? ' has-error' : '' }}">
            <label  class="control-label">Permission</label>
            <div class="col-md-12">
            <select name="permission" class="form-control select2">
                <optgroup label="Permission">
                    <option value="Admin" @if ($data->permission == 'Admin') selected="selected" @endif >Admin</option>
                    <option value="Author" @if ($data->permission == 'Author') selected="selected" @endif>Author</option>
                </optgroup>
            </select>
            @if ($errors->has('permission'))
                <span class="help-block">
                    <strong>{{ $errors->first('permission') }}</strong>
                </span>
            @endif
            </div>
        </div>
        </div>
        </div>
            <div class="modal-footer">
                <button type="button" class="btn blue btn-outline" data-dismiss="modal">Close</button>
                <button type="submit" class="btn green">Save changes</button>
            </div>
        </form>
        </div>
    </div>
</div>