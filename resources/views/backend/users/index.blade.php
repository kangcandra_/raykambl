@extends('layouts.backend')
@section('css')
    {!! Minify::stylesheet(['/assets/backend/global/plugins/select2/css/select2.min.css']) !!}
    {!! Minify::stylesheet(['/assets/backend/global/plugins/select2/css/select2-bootstrap.min.css']) !!}
    <link href="{{ asset('assets/backend/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/backend/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}"
        rel="stylesheet" type="text/css" />
@endsection

@section('js')
    {!! Minify::javascript('/assets/backend/pages/scripts/ui-modals.min.js') !!}
    {!! Minify::javascript('/assets/backend/global/plugins/jquery-ui/jquery-ui.min.js') !!}
    {!! Minify::javascript('/assets/backend/global/plugins/select2/js/select2.full.min.js') !!}
    {!! Minify::javascript('/assets/backend/pages/scripts/components-select2.min.js') !!}
@endsection

@section('title')
    <i class="icon-settings"></i> <span class="title">Users Management</span>
@endsection
@section('subtitle')
    <i class="icon-settings"></i>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-bubble font-green-sharp"></i>
                <span class="caption-subject font-green-sharp sbold">Users Management</span>
            </div>
            <div class="actions">
                <a class="btn purple btn btn-circle sbold" data-toggle="modal" href="#tambah"> <i class="fa fa-plus"></i>
                    Add </a>
                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
            </div>
            @include('backend.users._modal-add')
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Permission</th>
                        <th colspan="3"> Action </th>
                    </tr>
                </thead>
                <tbody>
                    @php $no=1; @endphp
                    @foreach ($users as $data)
                        @if ($loop->first)
                            <tr>
                                <td> {{ $no++ }} </td>
                                <td> {{ $data->name }} </td>
                                <td> {{ $data->email }} </td>
                                <td> {{ $data->permission }} </td>
                                <td><a class="btn yellow btn btn-circle sbold" data-toggle="modal"
                                        href="#edit{{ $data->id }}"> <i class="fa fa-pencil"></i> Edit </a>
                                </td>
                                <td></td>
                            </tr>
                        @else
                            <tr>
                                <td> {{ $no++ }} </td>
                                <td> {{ $data->name }} </td>
                                <td> {{ $data->email }} </td>
                                <td> {{ $data->permission }} </td>
                                <td><a class="btn yellow btn btn-circle sbold" data-toggle="modal"
                                        href="#edit{{ $data->id }}"> <i class="fa fa-pencil"></i> Edit </a>
                                </td>
                                <td>
                                    {!! Form::model($data, ['route' => ['backend.users.destroy', $data->id], 'method' => 'delete', 'class' => 'form-inline']) !!}
                                    {!! Form::button('<i class="fa fa-trash">  Delete </i>', ['type' => 'submit', 'class' => 'btn red btn btn-circle sbold js-submit-confirm']) !!}
                                    {!! Form::close() !!}
                                </td>
                                <td></td>
                            </tr>
                        @endif
                        @include('backend.users._modal-edit')
                    @endforeach
                </tbody>
            </table>
            <center>{{ $users->links() }}</center>
        </div>
    </div>

    @push('scripts')
        <script>
            $('#single').select2();
        </script>
    @endpush
@endsection
