@guest
<div class="mCustomScrollbar" data-mcs-theme="dark">
    <div class="popup right-menu">
        <div class="right-menu-wrap">
            <div class="user-menu-close js-close-aside">
                <a href="#" class="user-menu-content  js-clode-aside">
                    <span></span>
                    <span></span>
                </a>
            </div>

            <div class="logo">
                <a href="index-2.html" class="full-block-link"></a>
                <img src="{{ asset('assets/front/images/logo.png')}}" alt="Seosight" style="width: 250px; height: 70px;">
                <div class="logo-text">
                    {{--  <div class="logo-title">Seosight</div>  --}}
                </div>
            </div>

            <p class="text">Investigationes demonstraverunt lectores legere me lius quod
                ii legunt saepius est etiam processus dynamicus.
            </p>

        </div>

        <div class="widget login">

            <h4 class="login-title">Sign In to Your Account</h4>
            <form action="{{ url('login') }}" method="POST">
                @csrf
                <input class="email input-standard-grey" placeholder="Username or Email" type="email" name="email" required>
                <input class="password input-standard-grey" placeholder="Password" type="password" name="password">
                <div class="login-btn-wrap">

                    {{--  <div class="btn btn-medium btn--dark btn-hover-shadow">  --}}
                        <button class="btn btn-medium btn--dark btn-hover-shadow" type="submit">
                            <span class="text">login now</span>
                        </button>
                        
                        <span class="semicircle"></span>
                    {{--  </div>  --}}
                
                    <div class="remember-wrap">

                        <div class="checkbox">
                            <input id="remember" type="checkbox" name="remember" value="remember">
                            <label for="remember">Remember Me</label>
                        </div>

                    </div>

                </div>
            </form>
            <div class="helped">Lost your password?</div>
            <div class="helped">Register Now</div>

        </div>



        <div class="widget contacts">

            <h4 class="contacts-title">Get In Touch</h4>
            <p class="contacts-text">Lorem ipsum dolor sit amet, duis metus ligula amet in purus,
                vitae donec vestibulum enim, tincidunt massa sit, convallis ipsum.
            </p>

            <div class="contacts-item">
                <img src="{{ asset('assets/front/img/contact4.png') }}" alt="phone">
                <div class="content">
                    <a href="#" class="title">8 800 567.890.11</a>
                    <p class="sub-title">Mon-Fri 9am-6pm</p>
                </div>
            </div>

            <div class="contacts-item">
                <img src="{{ asset('assets/front/img/contact5.png') }}" alt="phone">
                <div class="content">
                    <a href="#" class="title">info@seosight.com</a>
                    <p class="sub-title">online support</p>
                </div>
            </div>

            <div class="contacts-item">
                <img src="{{ asset('assets/front/img/contact6.png') }}" alt="phone">
                <div class="content">
                    <a href="#" class="title">Melbourne, Australia</a>
                    <p class="sub-title">795 South Park Avenue</p>
                </div>
            </div>

        </div>

    </div>
</div>
@endguest