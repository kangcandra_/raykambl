<nav id="primary-menu" class="primary-menu">

    <a href='javascript:void(0)' id="menu-icon-trigger" class="menu-icon-trigger showhide">
        <span class="mob-menu--title">Menu</span>
        <span id="menu-icon-wrapper" class="menu-icon-wrapper" style="visibility: hidden">
            <svg width="1000px" height="1000px">
                <path id="pathD" d="M 300 400 L 700 400 C 900 400 900 750 600 850 A 400 400 0 0 1 200 200 L 800 800"></path>
                <path id="pathE" d="M 300 500 L 700 500"></path>
                <path id="pathF" d="M 700 600 L 300 600 C 100 600 100 200 400 150 A 400 380 0 1 1 200 800 L 800 200"></path>
            </svg>
        </span>
    </a>

    <!-- menu-icon-wrapper -->

    <ul class="primary-menu-menu">
        <li class="menu-item-has-children">
            <a href="{{ url('/') }}">Home</a>
        </li>
        <li class="">
            <a href="{{ url('/about') }}">About</a>
        </li>
        <li class="">
        <a href="{{ url('/service') }}">Services<span class="indicator"></span></a>
        <ul class="dropdown">
            <li class="megamenu-item-info">
                <h5 class="megamenu-item-info-title">List of Services</h5>
                <p class="megamenu-item-info-text">Choose a Service</p>
            </li>
            <li class="hover-ver2">
                <a href="05_service_details_localseo.html"><i class="seoicon-pin-map"></i>
                    Local SEO
                </a>
            </li>
            <li class="hover-ver2">
                <a href="07_service_email_marketing.html"><i class="seoicon-mail-send"></i>
                    Email Marketing
                </a>
            </li>
            <li class="hover-ver2">
                <a href="06_service_detail.html"><i class="seoicon-chat-comment"></i>
                    Social Media Marketing
                </a>
            </li>
            <li class="menu-item-has-children hover-ver2">
                <a href="04_service_detail_seo.html">
                    <i class="seoicon-search"></i>
                    Search Engine Optimization
                </a>
            </li>
            <li class="hover-ver2">
                <a href="08_service_ppc_management.html"><i class="seoicon-button"></i>
                    Pay Per Click Management
                </a>
            </li>
            <li class="menu-item-has-children hover-ver2">
                <a href="03_services.html">
                    <i class="seoicon-pie-graph-split"></i>
                    Services Promo List
                </a>
            </li>
        </ul>
        </li>
        <li class="">
            <a href="{{ url('/product') }}">Product</a>
        </li>
        <li class="">
            <a href="{{ url('portofolio') }}">
                Portofolio
            </a>
        </li>
        <li class="">
            <a href="{{ url('testimonial') }}">
                Testimonial
            </a>
        </li>
        {{-- <li class="">
            <a href="{{ url('/blog') }}">Blog</a>
        </li> --}}
        
        <li class="">
            <a href="{{ url('/contact') }}">Contact</a>
        </li>
    </ul>
</nav>
 <ul class="nav-add">
    <li class="cart">
        <a href="#" class="js-cart-animate">
            <i class="seoicon-basket"></i>
            @if(Auth::guest())
                <span class="cart-count">0</span>    
            @else
            <span class="cart-count">{{ count($cart) }}</span>
            @endif
        </a>
       
        <div class="cart-popup-wrap">
            <div class="popup-cart">
                @if(Auth::guest())
                @else
                @if(count($cart) > 1 )
                @foreach ($cart as $data)
                <img src="{{ asset('/assets/img/product/'.$data->photo.'') }}" alt="product" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" style="width:25px; height:25px;">
                <h4 class="title-cart">{{ $data->product_name }}</h4>
                <p class="subtitle">Model : {{ $data->model }}, Rp.{{ number_format($data->price,2,".",".") }}</p>
                @endforeach
                @else
                <h4 class="title-cart">No products in the cart!</h4>
                <p class="subtitle">Please make your choice.</p>
                @endif
                @endif
                <div class="btn btn-small btn--dark">
                    <a href="{{ url('cart') }}"><span class="text">view all catalog</span></a>
                </div>
            </div>
        </div>
    </li>
    <li class="search search_main">
        <a href="#" class="js-open-search">
            <i class="seoicon-loupe"></i>
        </a>
    </li>

</ul>