<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item start {{ Request::is('admin') ? 'active open' : '' }}">
                <a href="{{ url('/admin') }}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">Features</h3>
            </li>
            @if (Auth::user()->permission === 'Admin')
                <li class="nav-item  {{ Request::is('admin/users*') ? 'active open' : '' }}">
                    <a href="{{ route('backend.users.index') }}" class="nav-link nav-toggle">
                        <i class="icon-users"></i>
                        <span class="title">Manajemen User</span>
                        {{-- <span class="arrow"></span> --}}
                    </a>
                </li>
            @endif
            <li class="nav-item {{ Request::is('admin/post*') ? 'active open' : '' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-puzzle"></i>
                    <span class="title">Blog</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  {{ Request::is('admin/post') ? 'active open' : '' }}">
                        <a href="{{ route('backend.post.index') }}" class="nav-link">
                            <span class="title">Daftar Blog</span>
                            <span class="badge badge-success">{{ count($post) }}</span>
                        </a>
                    </li>
                    <li class="nav-item  {{ Request::is('admin/post_categories') ? 'active open' : '' }}">
                        <a href="{{ route('backend.post_categories.index') }}" class="nav-link">
                            <span class="title">Kategori</span>
                            <span class="badge badge-warning">{{ count($category) }}</span>
                        </a>
                    </li>
                    <li class="nav-item  {{ Request::is('admin/post_tags') ? 'active open' : '' }}">
                        <a href="{{ route('backend.post_tags.index') }}" class="nav-link">
                            <span class="title">Tag</span>
                            {{-- <span class="badge badge-danger">{{ count($tag) }}</span> --}}
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  {{ Request::is('admin/product*') ? 'active open' : '' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-briefcase"></i>
                    <span class="title">Produk</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  {{ Request::is('admin/product') ? 'active open' : '' }}">
                        <a href="{{ route('backend.product.index') }}" class="nav-link">
                            <span class="title">Daftar Produk</span>
                            <span class="badge badge-success">{{ count($product) }}</span>
                        </a>
                    </li>
                    <li class="nav-item  {{ Request::is('admin/product_categories') ? 'active open' : '' }}">
                        <a href="{{ route('backend.product_categories.index') }}" class="nav-link">
                            <span class="title">Kategori Produk</span>
                            <span class="badge badge-warning">{{ count($product_categories) }}</span>
                        </a>
                    </li>
                    <li class="nav-item  {{ Request::is('admin/product_tag') ? 'active open' : '' }}">
                        <a href="{{ route('backend.product_tag.index') }}" class="nav-link">
                            <span class="title">Tag Produk</span>
                            <span class="badge badge-danger">{{ count($product_tag) }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  {{ Request::is('admin/portofolio*') ? 'active open' : '' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-tag"></i>
                    <span class="title">Portofolio</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  {{ Request::is('admin/portofolio') ? 'active open' : '' }}">
                        <a href="{{ route('backend.portofolio.index') }}" class="nav-link">
                            <span class="title">Daftar Portofolio</span>
                            <span class="badge badge-success">{{ count($portofolio) }}</span>
                        </a>
                    </li>
                    <li class="nav-item  {{ Request::is('admin/portofolio_categories') ? 'active open' : '' }}">
                        <a href="{{ route('backend.portofolio_categories.index') }}" class="nav-link">
                            <span class="title">Kategori</span>
                            <span class="badge badge-warning">{{ count($portofoliocategories) }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  {{ Request::is('admin/client*') ? 'active open' : '' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-wallet"></i>
                    <span class="title">Klien</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  {{ Request::is('admin/client') ? 'active' : '' }}">
                        <a href="{{ route('backend.client.index') }}" class="nav-link">
                            <span class="title">Daftar Klien</span>
                            <span class="badge badge-success">{{ count($client) }}</span>
                        </a>
                    </li>
                    <li class="nav-item  {{ Request::is('admin/client_categories') ? 'active open' : '' }}">
                        <a href="{{ route('backend.client_categories.index') }}" class="nav-link">
                            <span class="title">Category</span>
                            <span class="badge badge-warning">{{ count($client_categories) }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  {{ Request::is('admin/team*') ? 'active open' : '' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-bulb"></i>
                    <span class="title">Tim Assalaam Studio</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  {{ Request::is('admin/team') ? 'active' : '' }}">
                        <a href="{{ route('backend.team.index') }}" class="nav-link">
                            <span class="title">Daftar Tim</span>
                            <span class="badge badge-success">{{ count($team) }}</span>
                        </a>
                    </li>
                    <li class="nav-item  {{ Request::is('admin/team_division') ? 'active open' : '' }}">
                        <a href="{{ route('backend.team_division.index') }}" class="nav-link">
                            <span class="title">Divisi Kerja</span>
                            <span class="badge badge-warning">{{ count($division) }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  {{ Request::is('admin/testimonial') ? 'active open' : '' }}">
                <a href="{{ url('/admin/testimonial') }}" class="nav-link nav-toggle">
                    <i class="icon-social-dribbble"></i>
                    <span class="title">Testmonial</span>
                    {{-- <span class="arrow"></span> --}}
                </a>
            </li>
        </ul>
    </div>
</div>
